/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.logic;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Boogaboo
 */
public class DEXFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return false;
        }
        String extension = Utils.getExtension(f);
        if (extension != null) {
            if (extension.equals(Utils.ser) || extension.equals(Utils.dexx0)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "DEX Method files (.dexx0, .ser)";
    }
    
    
    static class Utils {

        public final static String ser = "ser";
        public final static String dexx0 = "dexx0";

        /*
         * Get the extension of a file.
         */  
        public static String getExtension(File f) {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 &&  i < s.length() - 1) {
                ext = s.substring(i+1).toLowerCase();
            }
            return ext;
        }
    }
    
}
