/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.logic;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Boogaboo
 */
public class StateHandler {
    
    /**
     * @param object 
     * @param fileName "MyWorkModel.ser"
     */
    public static boolean saveWorkModel(WorkModel object, String fileName) {
        try {
            // Serialize data object to a file
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
            out.writeObject(object);
            out.close();
        /*
            // Serialize data object to a byte array
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(bos);
            out.writeObject(object);
            out.close();

            // Get the bytes of the serialized object
            byte[] buf = bos.toByteArray();
            
            //WorkModel newWorkModel = (WorkModel) 
        */
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public static WorkModel readSavedWorkModel(String fileName) {
        try {
            FileInputStream door = new FileInputStream(fileName); //"MyWorkModel.ser"
            ObjectInputStream reader = new ObjectInputStream(door);
            WorkModel oldWorkModel = new WorkModel();
            oldWorkModel = (WorkModel) reader.readObject();
            return oldWorkModel;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StateHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
}
