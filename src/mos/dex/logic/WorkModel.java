/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.logic;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.tree.DefaultTreeModel;
import mos.dex.model.Alternative;
import mos.dex.model.Attribute;
import mos.dex.model.Evaluation;
import mos.dex.model.Scale;
import mos.dex.model.UtilityFunction;

/**
 *
 * @author Boogaboo
 */
public class WorkModel implements Serializable {
    
    private File currentWorkFile;
    
    private DefaultTreeModel treeModel;
    private Evaluation e = new Evaluation(0, 0);
    
    private ArrayList<Attribute> attributes;
    private ArrayList<Alternative> alternatives;
    private ArrayList<Scale> scales;
    private ArrayList<UtilityFunction> utilFunctions;
    
    private int nodeCounter = 1;
    private int scaleCounter = 1;
    private int alternativeCounter = 1;
    private int utilityFunction = 1;
    
    public WorkModel() {
        this.attributes = new ArrayList<Attribute>();
        this.alternatives = new ArrayList<Alternative>();
        this.scales = new ArrayList<Scale>();
        this.utilFunctions = new ArrayList<UtilityFunction>();
    }

    /**
     * @return the currentWorkFile
     */
    public File getCurrentWorkFile() {
        return currentWorkFile;
    }

    /**
     * @return the e
     */
    public Evaluation getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(Evaluation e) {
        this.e = e;
    }

    /**
     * @param currentWorkFile the currentWorkFile to set
     */
    public void setCurrentWorkFile(File currentWorkFile) {
        this.currentWorkFile = currentWorkFile;
    }

    /**
     * @return the treeModel
     */
    public DefaultTreeModel getTreeModel() {
        return treeModel;
    }

    /**
     * @param treeModel the treeModel to set
     */
    public void setTreeModel(DefaultTreeModel treeModel) {
        this.treeModel = treeModel;
    }

    /**
     * @return the nodeCounter
     */
    public int getNodeCounter() {
        return nodeCounter;
    }

    /**
     * @param nodeCounter the nodeCounter to set
     */
    public void setNodeCounter(int nodeCounter) {
        this.nodeCounter = nodeCounter;
    }

    /**
     * @return the scaleCounter
     */
    public int getScaleCounter() {
        return scaleCounter;
    }

    /**
     * @param scaleCounter the scaleCounter to set
     */
    public void setScaleCounter(int scaleCounter) {
        this.scaleCounter = scaleCounter;
    }

    /**
     * @return the alternativeCounter
     */
    public int getAlternativeCounter() {
        return alternativeCounter;
    }

    /**
     * @param alternativeCounter the alternativeCounter to set
     */
    public void setAlternativeCounter(int alternativeCounter) {
        this.alternativeCounter = alternativeCounter;
    }

    /**
     * @return the utilityFunction
     */
    public int getUtilityFunction() {
        return utilityFunction;
    }

    /**
     * @param utilityFunction the utilityFunction to set
     */
    public void setUtilityFunction(int utilityFunction) {
        this.utilityFunction = utilityFunction;
    }
    

    /**
     * @return the attributes
     */
    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(ArrayList<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the alternatives
     */
    public ArrayList<Alternative> getAlternatives() {
        return alternatives;
    }

    /**
     * @param alternatives the alternatives to set
     */
    public void setAlternatives(ArrayList<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    /**
     * @return the scales
     */
    public ArrayList<Scale> getScales() {
        return scales;
    }

    /**
     * @param scales the scales to set
     */
    public void setScales(ArrayList<Scale> scales) {
        this.scales = scales;
    }

    /**
     * @return the utilFunctions
     */
    public ArrayList<UtilityFunction> getUtilFunctions() {
        return utilFunctions;
    }

    /**
     * @param utilFunctions the utilFunctions to set
     */
    public void setUtilFunctions(ArrayList<UtilityFunction> utilFunctions) {
        this.utilFunctions = utilFunctions;
    }
    
    
    
}
