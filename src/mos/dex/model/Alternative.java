/*Git test
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author Boogaboo
 */
public class Alternative implements Serializable {
    
    private int id;
    private String name;
    //VSTAVI SE ATTRIBUTE NAME KOT KEY IN SCALEPAIR VALUE KOT VALUE
    private LinkedList<Map.Entry<Attribute, String>> values;

    public Alternative() {
        this.id = -1;
        this.name = "New Alternative";
        this.values = new LinkedList<Map.Entry<Attribute, String>>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the values
     */
    public LinkedList<Map.Entry<Attribute, String>> getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(LinkedList<Map.Entry<Attribute, String>> values) {
        this.values = values;
    }
    
}
