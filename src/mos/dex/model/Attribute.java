/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.model;

import java.io.Serializable;

/**
 *
 * @author Boogaboo
 */
public class Attribute implements Serializable {
    
    private int id;
    private int id_parent;
    private int id_scale;
    private String name;
    private String description;

    public Attribute() {
        this.id = 0;
        this.id_parent = 0;
        this.id_scale = -1;
        this.name = "New attribute";
        this.description = "New attribute's description";
    }

    public Attribute(int id, int id_parent, int id_scale, String name, String description) {
        this.id = id;
        this.id_parent = id_parent;
        this.id_scale = id_scale;
        this.name = name;
        this.description = description;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public String getIDRepresentationString() {
        return "ID="+this.id+" ID_P="+this.id_parent+" ID_S="+this.id_scale;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_parent
     */
    public int getId_parent() {
        return id_parent;
    }

    /**
     * @param id_parent the id_parent to set
     */
    public void setId_parent(int id_parent) {
        this.id_parent = id_parent;
    }

    /**
     * @return the id_scale
     */
    public int getId_scale() {
        return id_scale;
    }

    /**
     * @param id_scale the id_scale to set
     */
    public void setId_scale(int id_scale) {
        this.id_scale = id_scale;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
