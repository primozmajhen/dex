/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.model;

import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Boogaboo
 */
public class Scale implements Serializable {
    
    private int id;
    private String name;
    private LinkedList<ScalePair> scaleValuePairs;

    public Scale() {
        this.id = 0;
        this.name = "Scale Name";
        this.scaleValuePairs = new LinkedList<ScalePair>();
    }
    
    @Override
    public String toString() {
        return this.getName() + " " + this.getId();
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the scaleValuePairs
     */
    public LinkedList<ScalePair> getScaleValuePairs() {
        return scaleValuePairs;
    }

    /**
     * @param scaleValuePairs the scaleValuePairs to set
     */
    public void setScaleValuePairs(LinkedList<ScalePair> scaleValuePairs) {
        this.scaleValuePairs = scaleValuePairs;
    }
    
}
