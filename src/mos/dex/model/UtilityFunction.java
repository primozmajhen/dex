/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Boogaboo
 */
public class UtilityFunction implements Serializable {
    
    private int id;
    private int id_attribute;
    private String description;
    private ArrayList<String> values;      //beležijo se le shranjene vrednosti

    public UtilityFunction() {
        System.out.println("Ustvarjen nov utility function brez vseh atributov.");
    }

    public UtilityFunction(int id, int id_attribute) {
        this.id = id;
        this.id_attribute = id_attribute;
        this.description = "Utility Function Description";
        this.values = new ArrayList<String>();
    }
    
    public String outputValues() {
        String output = this.toString() + "\r\n";
        for (int i=0; i<this.values.size(); i++) {
            output += "value i="+ i + "  |  " + this.values.get(i) + "\r\n";
        }
        return output + this.toString();
    }

    @Override
    public String toString() {
        return "ID="+id+" ID_Att="+id_attribute+"   "+description;
    }
    
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the id_attribute
     */
    public int getId_attribute() {
        return id_attribute;
    }

    /**
     * @param id_attribute the id_attribute to set
     */
    public void setId_attribute(int id_attribute) {
        this.id_attribute = id_attribute;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the values
     */
    public ArrayList<String> getValues() {
        return values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(ArrayList<String> values) {
        this.values = values;
    }
    
    
}
