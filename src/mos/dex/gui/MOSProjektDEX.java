/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.gui;

import mos.dex.logic.WorkModel;

/**
 *
 * @author Boogaboo
 */
public class MOSProjektDEX {
    
    private static MOSProjektDEX instance;
    
    private static DEXMethodApplication app;
    
    public WorkModel wModel;
    
    public static MOSProjektDEX getInstance() {
        if (instance == null) {
            instance = new MOSProjektDEX();
        }
        return instance;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    SwingUtilities.updateComponentTreeUI();
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        
        getInstance().app = new DEXMethodApplication();
        getInstance().app.setVisible(true);
        
        
        
    }

    /**
     * @return the wModel
     */
    public WorkModel getwModel() {
        return wModel;
    }

    /**
     * @param awModel the wModel to set
     */
    public void setwModel(WorkModel awModel) {
        wModel = awModel;
    }
    
}
