/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mos.dex.gui;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import mos.dex.logic.DEXFilter;
import mos.dex.logic.StateHandler;
import mos.dex.logic.WorkModel;
import mos.dex.model.Alternative;
import mos.dex.model.Attribute;
import mos.dex.model.Evaluation;
import mos.dex.model.Scale;
import mos.dex.model.ScalePair;
import mos.dex.model.UtilityFunction;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.TextAnchor;

/**
 *
 * @author Boogaboo
 */
public class DEXMethodApplication extends javax.swing.JFrame {
    
    private MOSProjektDEX ogrodje;
    
    private ArrayList<Attribute> at = null;
    private ArrayList<Alternative> al = null;
    private ArrayList<Scale> sc = null;
    private ArrayList<UtilityFunction> uf = null;
    
    private int returnValue = -1;
    private DEXFilter dexFilter;
    private ImageIcon barChartIcon;
    
    private DefaultComboBoxModel<Scale> myComboBoxScaleModel = null;
    private DefaultComboBoxModel<ScalePair> myComboBoxAlternativeScalePairModel = null;
    private DefaultComboBoxModel<String> myComboBoxUtilFnModel = null;
    private DefaultTableModel myScaleTableModel = null;
    private DefaultTableModel myUtilFnTableModel = null;
    private DefaultTableModel myEvaluationTableModel = null;
    private DefaultTableModel myTableModel = null;
    
    
    /**
     * Creates new form DEXMethodApplication
     */
    public DEXMethodApplication() {
        ogrodje = MOSProjektDEX.getInstance();
        dexFilter = new DEXFilter();
        ogrodje.setwModel(new WorkModel());
//        String path = "";
//        try {
//            path = getProgramPath();
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(DEXMethodApplication.class.getName()).log(Level.SEVERE, null, ex);
//        }
        String newFileName = "DEXMethodEvaluation.dexx0";
        ogrodje.getwModel().setCurrentWorkFile(new File(newFileName));
        
        initComponents();
        setTitle("DEXMethod - " + newFileName);
        initComponents2();
        
        refreshAttributes();
    }
    
    private void initComponents2() {
        
        initComponents3();
        
        jTree2.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        jTree2.setExpandsSelectedPaths(true);
        jTree2.setEditable(false);
        jTree2.setShowsRootHandles(false);
        
        DefaultTreeModel treeModel = (DefaultTreeModel) jTree2.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel.getRoot();
        int stOtrok = root.getChildCount();
        System.out.println("stOtrok="+stOtrok);
        
        if (stOtrok == 0) {
            if (root.getUserObject() instanceof String) {
                Attribute a = new Attribute(0, -1, -1, "Root Attribute", "Description");
                root.setUserObject(a);
                ogrodje.getwModel().getAttributes().add(a);
                jTree2.setModel(new DefaultTreeModel(root, true));
                DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getModel().getRoot();
                jTree2.setSelectionPath(new TreePath(selectedNode));
            }
        }
        
        expandAllNodes(0, jTree2.getRowCount());
        
    }
    
    private void initComponents3() {
        jTable1Alternatives.setModel(createMyTableModel());
        jComboBox1ScaleNames.setModel(createMyComboBoxScaleModel());
    }
    
    private void refreshReferencesToCollections() {
        at = ogrodje.getwModel().getAttributes();
        al = ogrodje.getwModel().getAlternatives();
        sc = ogrodje.getwModel().getScales();
        uf = ogrodje.getwModel().getUtilFunctions();
    }
    
    private void refreshScaleIDtoAllAttributesInAllAlternatives(int newScaleID, Attribute attInQuestion) {
        refreshReferencesToCollections();
        for (int i=0; i<al.size(); i++) {
            for (int j=0; j<al.get(i).getValues().size(); j++) {
                if (al.get(i).getValues().get(j).getKey().getId() == attInQuestion.getId()) {
                    ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).getKey().setId_scale(newScaleID);
                    //String defaultScaleValue = "."; TUKAJ BI LAHKO NASTAVIL DEFAULT VREDNOST - scale pair
                    ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).setValue(".");
                }
            }
        }
    }
    
    /** Remove the currently selected node. */
    public void removeCurrentNode() {
        TreePath currentSelection = jTree2.getSelectionPath();
        if (currentSelection != null) {
            DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection.getLastPathComponent());
            jLabel4StatusBarLabel.setText("<html>" + ((Attribute)currentNode.getUserObject()).getName() + " removed" + "</html>");
            MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
            if (parent != null) {
                
                removeChildrenAndAttributes(currentNode);
                
            }
        }
        if (jTree2.getRowCount() != ogrodje.getwModel().getAttributes().size()) {
            System.out.println("TEŽAVA!!! ---> jTree2.getRowCount() = " + jTree2.getRowCount() + " --- atts.size = " + ogrodje.getwModel().getAttributes().size());
        }
    }
    
    private void removeChildrenAndAttributes(DefaultMutableTreeNode currentNode) {
        if (currentNode.getChildCount() > 0) {
            
            removeAttributes((Attribute)currentNode.getUserObject());
            jLabel4StatusBarLabel.setText("<html>" + ((Attribute)currentNode.getUserObject()).getName() + " and it's children removed" + "</html>");
        } else {
            ogrodje.getwModel().getAttributes().remove((Attribute)currentNode.getUserObject());
        }
        ((DefaultTreeModel)jTree2.getModel()).removeNodeFromParent((MutableTreeNode)currentNode);
    }
    
    private void removeAttributes(Attribute parentAttribute) {
        int idParent = parentAttribute.getId();
        ogrodje.getwModel().getAttributes().remove(parentAttribute);
        Attribute cAtt = null;
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            cAtt = ogrodje.getwModel().getAttributes().get(i);
            if (cAtt.getId_parent() == idParent) {
                
                i--;
                removeAttributes(cAtt);
            }
        }
    }
    
    private void removeChildrenAndAttributes2(DefaultMutableTreeNode currentNode) {
        Attribute childAttribute;
        DefaultMutableTreeNode childNode;
        for (int i=0; i<currentNode.getChildCount(); i++) {
                
            childNode = (DefaultMutableTreeNode) currentNode.getChildAt(i);
            if (childNode.getChildCount() > 0) {
                removeChildrenAndAttributes2(childNode);          //REKURZIJA!!!
            } else {
                childAttribute = (Attribute) childNode.getUserObject();
                ogrodje.getwModel().getAttributes().remove(childAttribute);
                ((DefaultTreeModel)jTree2.getModel()).removeNodeFromParent((MutableTreeNode)childNode);
            }

        }
        ogrodje.getwModel().getAttributes().remove((Attribute)currentNode.getUserObject());
        ((DefaultTreeModel)jTree2.getModel()).removeNodeFromParent((MutableTreeNode)currentNode);
        
    }

    /** Add child to the currently selected node.
     * @param child
     * @return  */
    public DefaultMutableTreeNode addObject(Object child) {
        DefaultMutableTreeNode parentNode = null;
        TreePath parentPath = jTree2.getSelectionPath();

        if (parentPath == null) {
            parentNode = (DefaultMutableTreeNode) ((DefaultTreeModel)jTree2.getModel()).getRoot();
        } else {
            parentNode = (DefaultMutableTreeNode) (parentPath.getLastPathComponent());
        }
        return addObject(parentNode, child, true);
    }

    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child) {
        return addObject(parent, child, false);
    }

    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child, boolean shouldBeVisible) {
        DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);

        if (parent == null) {//tukaj mora biti ID od Root noda/atributa enak 0, idParent od roota pa -1
            parent = (DefaultMutableTreeNode) ((DefaultTreeModel)jTree2.getModel()).getRoot();
            //otrok roota ima vedno idParent enak 0
            ((Attribute)((DefaultMutableTreeNode) child).getUserObject()).setId_parent(0);
            ogrodje.getwModel().setNodeCounter(1); //v primeru da je le root potem spremenim counter nazaj na 1
        } else {
            int idParent = ((Attribute)parent.getUserObject()).getId();
            Attribute a = (Attribute) childNode.getUserObject();
            a.setId_parent(idParent);
            childNode.setUserObject(a);
        }
	
	//It is key to invoke this on the TreeModel, and NOT DefaultMutableTreeNode
        ((DefaultTreeModel)jTree2.getModel()).insertNodeInto(childNode, parent, parent.getChildCount());
        //DODAMO ATRIBUT V SEZNAM
        ogrodje.getwModel().getAttributes().add((Attribute)childNode.getUserObject());
        
        //Make sure the user can see the lovely new node.
        if (shouldBeVisible) {
            jTree2.scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        return childNode;
    }
    
    /** Remove all nodes except the root node. */
    private void clearTree() {
        ((DefaultMutableTreeNode)((DefaultTreeModel)jTree2.getModel()).getRoot()).removeAllChildren();
        ((DefaultTreeModel)jTree2.getModel()).reload();
    }
    
    private void expandAllNodes(int startingIndex, int rowCount){
        for(int i=startingIndex; i<rowCount; ++i) {
            jTree2.expandRow(i);
        }
        if(jTree2.getRowCount() != rowCount){
            expandAllNodes(rowCount, jTree2.getRowCount());
        }
    }
    
    private void refreshAttributes() {
        String s = "";
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            s += ogrodje.getwModel().getAttributes().get(i).getIDRepresentationString() + "\r\n";
        }
        jTextArea1TEST.setText(s);
    }
    
    private DefaultTableModel createMyTableModel() {
        ArrayList<Attribute> atts = ogrodje.getwModel().getAttributes();
        ArrayList<Alternative> alts = ogrodje.getwModel().getAlternatives();
        myTableModel = new DefaultTableModel(atts.size()+1, alts.size()+1) {
            @Override
            public boolean isCellEditable(int row, int column) {
                //if (row == 0 || column == 0)     //samo prva vrstica in prvi stolpec
                    return false;                  //nič se ne ureja na roko
                //else
                //    return true;
            }
        };
        //myCustomTableModel.
        myTableModel.setValueAt("Option", 0, 0);
        //set Alternative names:
        for (int i=0; i<alts.size(); i++) {
            myTableModel.setValueAt(alts.get(i).getName(), 0, i+1);
        }
        //set Attribute names:
        String oznakaNivoja = "";
        int stNivoja = 0;
        //nastavi št. vrstice
        for (int i=0; i<atts.size(); i++) {
            oznakaNivoja = "";
            stNivoja = vrniSteviloNivojev(atts.get(i), 0);
            for (int temp=0; temp<stNivoja; temp++) {
                oznakaNivoja += "-";
            }
            myTableModel.setValueAt(oznakaNivoja + atts.get(i).getName(), i+1, 0);
        }
        
        
        //fill in alternative scalepair string values
        for (int i=0; i<alts.size(); i++) {
            for (int j=0; j<atts.size(); j++) {
                                                                /*atts.get(j).getName()*/
                Attribute attScaleTest = alts.get(i).getValues().get(j).getKey();
                //if (attScaleTest.getId_scale() )
                    /*VGLAVNEM TU ALI TAM KJER SE SPREMINJA SCALE JE POTREBNO POSKRBETI, DA SE PRILAGODIJO TUDI POLJA vrednosti alternativ*/
                
                if (hasChildren(attScaleTest)) {
                    myTableModel.setValueAt(alts.get(i).getValues().get(j).getValue(), j + 1, i + 1);
                } else {
                    myTableModel.setValueAt(alts.get(i).getValues().get(j).getValue(), j + 1, i + 1);
                }
            }
        }
        return myTableModel;
    }
    
    private DefaultTableModel createMyScaleTableModel() {
        myScaleTableModel = new DefaultTableModel(1, 2) {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (row == 0)     //samo prva vrstica
                    return false;
                else
                    return true;
            }
        };
        myScaleTableModel.setValueAt("Value", 0, 0);
        myScaleTableModel.setValueAt("Description", 0, 1);
        //myScaleTableModel.setValueAt("Value 0", 1, 0);
        //myScaleTableModel.setValueAt("", 1, 1);
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a = null;
        Scale s = null;
        if (selectedNode != null) {

            if (jComboBox1ScaleNames.getSelectedIndex() == 0) {
                //open dialog and create new Scale
                
            } else {
                //edit set scale
                s = (Scale) jComboBox1ScaleNames.getSelectedItem();
                
                myScaleTableModel = new DefaultTableModel(s.getScaleValuePairs().size()+1, 2) {
                    @Override
                    public boolean isCellEditable(int row, int column) {
                        if (row == 0)     //samo prva vrstica
                            return false;
                        else
                            return true;
                    }
                };
                myScaleTableModel.setValueAt("Value", 0, 0);
                myScaleTableModel.setValueAt("Description", 0, 1);
                for (int i=0; i<s.getScaleValuePairs().size(); i++) {
                    myScaleTableModel.setValueAt(s.getScaleValuePairs().get(i).getName(), i+1, 0);
                    myScaleTableModel.setValueAt(s.getScaleValuePairs().get(i).getDescription(), i+1, 1);
                }
            }
        }
        return myScaleTableModel;
    }
    
    private DefaultTableModel createMyUtilFnTableModel(Attribute izbranA, ArrayList<Attribute> otrociA) {
        int stVrstic = 0;
        String uFname = "";
        Scale s = null;
        ArrayList<Scale> attsScales = new ArrayList<Scale>();
        for (int i=0; i<otrociA.size(); i++) {
            for (int j=0; j<ogrodje.getwModel().getScales().size(); j++) {
                if (ogrodje.getwModel().getScales().get(j).getId() == otrociA.get(i).getId_scale()) {
                    attsScales.add(ogrodje.getwModel().getScales().get(j));
                }
                if (s == null && izbranA.getId_scale() == ogrodje.getwModel().getScales().get(j).getId()) {
                    s = ogrodje.getwModel().getScales().get(j);
                }
            }
        } //pričakujem enako število scales kot je otrokA, pridobil sem tudi main scale s
        stVrstic = attsScales.get(0).getScaleValuePairs().size();
//        System.out.println("UtilFnDialogTable: stVrstic=" + stVrstic);
        for (int i=1; i<attsScales.size(); i++) {
            stVrstic *= attsScales.get(i).getScaleValuePairs().size();
//            System.out.println("UtilFnDialogTable: stVrstic=" + stVrstic + " *= attsScales.getScaleValuePairs.size="+ attsScales.get(i).getScaleValuePairs().size());
        }
        
        myUtilFnTableModel = new DefaultTableModel(stVrstic+1, otrociA.size()+2) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        //TUKAJ PA ZDAJ POGRUNTAM KAKO VRSTICE PRAVILNO NAPOLNITI
        int rows = myUtilFnTableModel.getRowCount();
        int cols = myUtilFnTableModel.getColumnCount();
        
        //nastavi št. vrstice
        for (int i=1; i<rows; i++) {
            myUtilFnTableModel.setValueAt(i, i, 0);
//            System.out.println("rows=" + rows + " i="+ i );
        }
        //nastavi nazive stolpcev
        for (int i=0; i<otrociA.size(); i++) {
            myUtilFnTableModel.setValueAt(otrociA.get(i).getName(), 0, i+1);
//            System.out.println("cols=" + cols + " i="+ i + " otrociA.get("+i+").getName="+otrociA.get(i).getName());
        }
        myUtilFnTableModel.setValueAt(izbranA.getName(), 0, cols-1);
//        System.out.println("izbranA.getName="+izbranA.getName()+ " v zadnjem columnu=cols="+cols+" cols-1="+(cols-1));
        
        //napolni vrednosti
        //v kolikor util fn že obstaja napolni obstoječe vrednosti ter vstavi privzeto pri ostalih
        //UTIL FN SE VEDNO USTVARI NA NOVO.     -- OD TOD DALJE
        UtilityFunction uF = null;
        int uFindex = -1;
        for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
            if (izbranA.getId() == ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute()) {
                uF = ogrodje.getwModel().getUtilFunctions().get(i);
                uFindex = i;
            }
        }
        
        //kreiraj description/name
        for (int i=0; i<otrociA.size(); i++) {
            uFname += otrociA.get(i).getName();
            if (i == (otrociA.size()-1)) {
                //nič
            } else {
                uFname += " | ";
            }
        }
        System.out.println("uFname="+uFname);
        
        if (uF == null) {
            //ustvari novo UtilFn
            int utFnId = ogrodje.getwModel().getUtilityFunction();
            uF = new UtilityFunction(utFnId, izbranA.getId());
            uF.setDescription(uFname);
            ogrodje.getwModel().setUtilityFunction(utFnId + 1);
        } else {
            //uporabi obstoječo UtilFn
            uF.setDescription(uFname);
        }
        
        if (uF.getValues().size() != rows-1) {
            uF.setValues(new ArrayList<String>(Collections.nCopies(rows-1, "")));
        }
        //napolni znane vrednosti iz UtilityFunctions ali napolni prazne stringe
        for (int i=0; i<rows-1; i++) {
            myUtilFnTableModel.setValueAt(uF.getValues().get(i), i+1, cols-1);
//            System.out.println("Nastavljen default value v vrstico "+(i+1));
        }
        if (uFindex == -1)
            ogrodje.getwModel().getUtilFunctions().add(uF);
        else
            ogrodje.getwModel().getUtilFunctions().set(uFindex, uF);
                                                                                                                            System.out.println("PRED PIZDARIJO!!!");
//  ----------------------------------------------------------------------------
        int [] scaleValuePairsSize = new int[otrociA.size()];
        for (int i=0; i<otrociA.size(); i++) {
            scaleValuePairsSize[i] = attsScales.get(i).getScaleValuePairs().size();
        }
        
        int z = 0;
        int t = 0;
        int row = 0;
        int temp = 0;
        for (int i=0; i<otrociA.size(); i++) {      //preidem vse atribute
            t = 1;
            for (int x=0; x<=i; x++) {
                t *= scaleValuePairsSize[x];
            }
            temp = stVrstic/t;
            
            System.out.println("i="+i+" t="+t+" temp="+temp);
            for (int j=1; j <= t; j++) {      //preidem vse razdelke v atributu

                for (int k=1; k <= temp; k++) {     //poiščem dejansko pravo vrednost
                    
                    z = (row / temp) % scaleValuePairsSize[i];
                    
//                    System.out.println("row="+row+" row/temp="+(row/temp)+" j="+j+" k="+k+" -- Z = (row-k="+(row-k)+" % scaleValuePairsSize[i="+i+"]) = "+((row-k)%scaleValuePairsSize[i]));
                    myUtilFnTableModel.setValueAt(attsScales.get(i).getScaleValuePairs().get(z).getName(), row+1, i+1);
                    row++;
                }
            }
            row = 0;
        }

        //po končanem vpisovanju vrednosti shrani novo utilFn v workmodel
        if (uFindex == -1) {
            ogrodje.getwModel().getUtilFunctions().add(uF);
        } else {
            ogrodje.getwModel().getUtilFunctions().set(uFindex, uF);
        }
        
        return myUtilFnTableModel;
    }
    
    /**Funkcija ki napolni polja v tabeli, ki se ji jo posreduje glede na znani algoritem.
     * Najprej za vsak atribut, ki je otrok izbranega (kateremu nato določimo/beremo UtilityFunction vrednosti,
     * poišče Scale in ga doda v array, izračuna št vrstic tabele, ter pridobi zaloge vrednosti.
     * Nato izpolni vse vrednosti v tabeli za vsak atribut.
     */
    public void napolniVrednostiAtributov(DefaultTableModel currentTable, Attribute izbranA, ArrayList<Attribute> otrociA) {
        int stVrstic = 0;
        Scale s = null;
        ArrayList<Scale> attsScales = new ArrayList<Scale>();
        for (int i=0; i<otrociA.size(); i++) {
            for (int j=0; j<ogrodje.getwModel().getScales().size(); j++) {
                if (ogrodje.getwModel().getScales().get(j).getId() == otrociA.get(i).getId_scale()) {
                    attsScales.add(ogrodje.getwModel().getScales().get(j));
                }
                if (s == null && izbranA.getId_scale() == ogrodje.getwModel().getScales().get(j).getId()) {
                    s = ogrodje.getwModel().getScales().get(j);
                }
            }
        } //pričakujem enako število scales kot je otrokA, pridobil sem tudi main scale s
        stVrstic = attsScales.get(0).getScaleValuePairs().size();
        System.out.println("UtilFnDialogTable: stVrstic=" + stVrstic);
        for (int i=1; i<attsScales.size(); i++) {
            stVrstic *= attsScales.get(i).getScaleValuePairs().size();
            System.out.println("UtilFnDialogTable: stVrstic=" + stVrstic + " *= attsScales.getScaleValuePairs.size="+ attsScales.get(i).getScaleValuePairs().size());
        }
        
        int [] scaleValuePairsSize = new int[otrociA.size()];
        for (int i=0; i<otrociA.size(); i++) {
            scaleValuePairsSize[i] = attsScales.get(i).getScaleValuePairs().size();
        }
        
        int z, t, temp;
        int row = 0;
        for (int i=0; i<otrociA.size(); i++) {      //preidem vse atribute
            t = 1;
            for (int x=0; x<=i; x++) {
                t *= scaleValuePairsSize[x];
            }
            temp = stVrstic/t;
            
            System.out.println("i="+i+" t="+t+" temp="+temp);
            for (int j=1; j <= t; j++) {      //preidem vse razdelke v atributu

                for (int k=1; k <= temp; k++) {     //poiščem dejansko pravo vrednost
                    
                    z = (row / temp) % scaleValuePairsSize[i];
                    
                    myUtilFnTableModel.setValueAt(attsScales.get(i).getScaleValuePairs().get(z).getName(), row+1, i+1);
                    row++;
                }
            }
            row = 0;
        }
    }
    
    /**Previdno, ker sploh ne uporabim pAtt, kar pomeni da iz tabele ne morem brat vrednosti UF za pAtt.
     */
    private String[][] izgradi2DArrayVrednostiZaEvaluacijo(Attribute pAtt, ArrayList<Attribute> childAtts) {
        
        int stVrstic = 0;
        Scale s = null;
        ArrayList<Scale> attsScales = new ArrayList<Scale>();
        for (int i=0; i<childAtts.size(); i++) {
            for (int j=0; j<ogrodje.getwModel().getScales().size(); j++) {
                if (ogrodje.getwModel().getScales().get(j).getId() == childAtts.get(i).getId_scale()) {
                    attsScales.add(ogrodje.getwModel().getScales().get(j));
                }
                if (s == null && pAtt.getId_scale() == ogrodje.getwModel().getScales().get(j).getId()) {
                    s = ogrodje.getwModel().getScales().get(j);
                }
            }
        } //pričakujem enako število scales kot je otrokA, pridobil sem tudi main scale s
        stVrstic = attsScales.get(0).getScaleValuePairs().size();
//        System.out.println("EvaluationTable: stVrstic=" + stVrstic);
        for (int i=1; i<attsScales.size(); i++) {
            stVrstic *= attsScales.get(i).getScaleValuePairs().size();
//            System.out.println("EvaluationTable: stVrstic=" + stVrstic + " *= attsScales.getScaleValuePairs.size="+ attsScales.get(i).getScaleValuePairs().size());
        }
        
        String[][] tabela = new String[stVrstic][childAtts.size()];             //TABELA!!!
        
        int [] scaleValuePairsSize = new int[childAtts.size()];
        for (int i=0; i<childAtts.size(); i++) {
            scaleValuePairsSize[i] = attsScales.get(i).getScaleValuePairs().size();
        }
        
        int z, t, temp;
        int row = 0;
        for (int i=0; i<childAtts.size(); i++) {      //preidem vse atribute
            t = 1;
            for (int x=0; x<=i; x++) {
                t *= scaleValuePairsSize[x];
            }
            temp = stVrstic/t;
            
            System.out.println("EVALUATION TABLE ata="+pAtt.toString()+"  otrok="+childAtts.get(i).getName()+"    i="+i+" t="+t+" temp="+temp);
            for (int j=1; j <= t; j++) {      //preidem vse razdelke v atributu

                for (int k=1; k <= temp; k++) {     //poiščem dejansko pravo vrednost
                    
                    z = (row / temp) % scaleValuePairsSize[i];
                    
                    tabela[row][i] = attsScales.get(i).getScaleValuePairs().get(z).getName();
                    row++;
                }
            }
            row = 0;
        }
        return tabela;
    }
    
    private DefaultTableModel createMyEvaluationTableModel() {
        myEvaluationTableModel = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        
        myEvaluationTableModel = new DefaultTableModel(at.size()+1, al.size()+1) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        //TUKAJ PA ZDAJ POGRUNTAM KAKO VRSTICE PRAVILNO NAPOLNITI
        int rows = myEvaluationTableModel.getRowCount();
        int cols = myEvaluationTableModel.getColumnCount();
        
        String oznakaNivoja = "";
        int stNivoja = 0;
        //nastavi št. vrstice
        for (int i=0; i<at.size(); i++) {
            oznakaNivoja = "";
            stNivoja = vrniSteviloNivojev(at.get(i), 0);
            for (int temp=0; temp<stNivoja; temp++) {
                oznakaNivoja += "-";
            }
//            System.out.println("oznakaNivoja: "+oznakaNivoja + "      atribut:"+at.get(i).getName());
            myEvaluationTableModel.setValueAt(oznakaNivoja + at.get(i).getName(), i+1, 0);
            
        }
        for (int j=0; j<al.size(); j++) {
            myEvaluationTableModel.setValueAt(al.get(j).getName(), 0, j+1);
        }
        
        //napolni vrednosti
        //v kolikor util fn že obstaja napolni obstoječe vrednosti ter vstavi privzeto pri ostalih
        //UTIL FN SE VEDNO USTVARI NA NOVO.     -- OD TOD DALJE
//        UtilityFunction uF = null;
//        for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
//            if (izbranA.getId() == ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute()) {
//                uF = ogrodje.getwModel().getUtilFunctions().get(i);
//            }
//        }
        Evaluation e = new Evaluation(at.size(), al.size());
        UtilityFunction uF = null;
        for (int i=0; i<at.size(); i++) {
            
            for (int k=0; k<uf.size(); k++) {
                if (at.get(i).getId() == uf.get(k).getId_attribute()) {
                    uF = uf.get(k);
                }
            }
            
            for (int j=0; j<al.size(); j++) {
                //poišči ustrezno vrednost v alternativi
                for (int l=0; l<al.get(j).getValues().size(); l++) {
                    if (al.get(j).getValues().get(l).getKey().getId() == at.get(i).getId()) {
                        //v scale nastavljen in v alternatives izbran value
                        String vrednost = al.get(j).getValues().get(l).getValue();
                        myEvaluationTableModel.setValueAt(vrednost, i+1, j+1);
                        e.t[i][j] = vrednost;
                    }
                }
            }
        }
        ogrodje.getwModel().setE(e);
        return myEvaluationTableModel;
    }
    
    private int vrniSteviloNivojev(Attribute a, int stevec) {
        if (a.getId_parent() == -1) {
            //tu je root
            return stevec;
        } else {
            for (int i=0; i<at.size(); i++) {
                if (a.getId_parent() == at.get(i).getId()) {
                    stevec = vrniSteviloNivojev(at.get(i), stevec + 1);     //REKURZIJA
                    break;
                }
            }
            return stevec;
        }
    }
    
    private DefaultComboBoxModel createMyComboBoxUtilFnModel(int vrstica, int attrID) {
        Scale s = null;
        myComboBoxUtilFnModel = new DefaultComboBoxModel<String>();
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            if (ogrodje.getwModel().getAttributes().get(i).getId() == attrID) {
                for (int j=0; j<ogrodje.getwModel().getScales().size(); j++) {
                    if (ogrodje.getwModel().getScales().get(j).getId() == ogrodje.getwModel().getAttributes().get(i).getId_scale()) {
                        s = ogrodje.getwModel().getScales().get(j);
                    }
                }
            }
        }
        for (int i=0; i<s.getScaleValuePairs().size(); i++) {
            myComboBoxUtilFnModel.addElement(s.getScaleValuePairs().get(i).getName());
        }
        return myComboBoxUtilFnModel;
    }
    
    private DefaultComboBoxModel createMyComboBoxScaleModel() {
        myComboBoxScaleModel = new DefaultComboBoxModel<Scale>();
        Scale dummyScale = new Scale();
        dummyScale.setName("Scale Undefined");
        myComboBoxScaleModel.addElement(dummyScale);
        for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
            myComboBoxScaleModel.addElement(ogrodje.getwModel().getScales().get(i));
        }
        return myComboBoxScaleModel;
    }
    
    private DefaultComboBoxModel createMyComboBoxAlternativeScalePairsModel() {
        myComboBoxAlternativeScalePairModel = new DefaultComboBoxModel<ScalePair>();
        int row = jTable1Alternatives.getSelectedRow();
        int col = jTable1Alternatives.getSelectedColumn();
        int stevecOtrok = 0;
        Alternative alt = null;
        Attribute att = null;
        Scale s = null;
        if (row != 0 && col != 0 && row != -1 && col != -1) {
            alt = ogrodje.getwModel().getAlternatives().get(col - 1);
            att = ogrodje.getwModel().getAttributes().get(row - 1);
            for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
                if (att.getId_scale() == ogrodje.getwModel().getScales().get(i).getId()) {
                    //preveri še ali je root element ali starševski element, takrat onemogoči editanje in ne dovoli vnašanja vrednosti
                    if (hasChildren(att)) {
                        ScalePair sp = new ScalePair();
                        sp.setName("Set Scales To It's Children");
                        myComboBoxAlternativeScalePairModel.addElement(sp);
                    } else {
                        s = ogrodje.getwModel().getScales().get(i);

                        for (int j=0; j<s.getScaleValuePairs().size(); j++) {
                            myComboBoxAlternativeScalePairModel.addElement(s.getScaleValuePairs().get(j));
                        }
                    }
                }
            }
        } else {
            ScalePair sp = new ScalePair();
            sp.setName("Invalid Selection");
            myComboBoxAlternativeScalePairModel.addElement(sp);
        }
        if (myComboBoxAlternativeScalePairModel.getSize() == 0) {
            ScalePair sp = new ScalePair();
            sp.setName("Set Scale To The Attribute First");
            myComboBoxAlternativeScalePairModel.addElement(sp);
        }
        
        return myComboBoxAlternativeScalePairModel;
    }
    
    private boolean hasChildren(Attribute a) {
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            if (a.getId() == ogrodje.getwModel().getAttributes().get(i).getId_parent()) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1SetScale = new javax.swing.JDialog();
        jPanel9 = new javax.swing.JPanel();
        jButton1AddScaleValuePair = new javax.swing.JButton();
        jButton2RemoveScaleValuePair = new javax.swing.JButton();
        jButton1OKSetScaleDialog = new javax.swing.JButton();
        jButton2CancelSetScaleDialog = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable1SetScale = new javax.swing.JTable();
        jDialog1SetUtilFn = new javax.swing.JDialog();
        jPanel12 = new javax.swing.JPanel();
        jComboBox1UtilFnPickValue = new javax.swing.JComboBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1UtilFunction = new javax.swing.JTable();
        jButton1OKSetUtilFnDialog = new javax.swing.JButton();
        jButton2CancelSetUtilFnDialog = new javax.swing.JButton();
        jLabel4UtilFnInfo = new javax.swing.JLabel();
        jButton1SetDefaultUtilFnValues = new javax.swing.JButton();
        jFileChooser1Open = new javax.swing.JFileChooser();
        jFileChooser1Save = new javax.swing.JFileChooser();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTree2 = new javax.swing.JTree();
        jButton1Add = new javax.swing.JButton();
        jButton2Remove = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jPanel7Attribute = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1AttName = new javax.swing.JTextField();
        jTextField2AttDesc = new javax.swing.JTextField();
        jPanel7Scale = new javax.swing.JPanel();
        jButton1SetScale = new javax.swing.JButton();
        jComboBox1ScaleNames = new javax.swing.JComboBox();
        jButton1ScaleRemove = new javax.swing.JButton();
        jPanel7UtilityFunction = new javax.swing.JPanel();
        jButton1SetUtilityFunction = new javax.swing.JButton();
        jTextField3UtilityFunction = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1TEST = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1Alternatives = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jButton1AddAlternative = new javax.swing.JButton();
        jButton1RemoveAlternative = new javax.swing.JButton();
        jComboBox2AttributeScaleValues = new javax.swing.JComboBox();
        jTextField1AlternativeRename = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable1Evaluation = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        jButton1Reevaluate = new javax.swing.JButton();
        jLabel4Info1 = new javax.swing.JLabel();
        jLabel5Info2 = new javax.swing.JLabel();
        jPanel4Chart = new javax.swing.JPanel();
        jLabel3Chart = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel4StatusBarLabel = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        cutMenuItem = new javax.swing.JMenuItem();
        copyMenuItem = new javax.swing.JMenuItem();
        pasteMenuItem = new javax.swing.JMenuItem();
        deleteMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentsMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        jDialog1SetScale.setTitle("Set Scale");
        jDialog1SetScale.setAlwaysOnTop(true);
        jDialog1SetScale.setMinimumSize(new java.awt.Dimension(364, 350));
        jDialog1SetScale.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        jDialog1SetScale.setResizable(false);
        jDialog1SetScale.setType(java.awt.Window.Type.UTILITY);

        jButton1AddScaleValuePair.setText("Add");
        jButton1AddScaleValuePair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1AddScaleValuePairActionPerformed(evt);
            }
        });

        jButton2RemoveScaleValuePair.setText("Remove");
        jButton2RemoveScaleValuePair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2RemoveScaleValuePairActionPerformed(evt);
            }
        });

        jButton1OKSetScaleDialog.setText("OK");
        jButton1OKSetScaleDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1OKSetScaleDialogActionPerformed(evt);
            }
        });

        jButton2CancelSetScaleDialog.setText("Cancel");
        jButton2CancelSetScaleDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2CancelSetScaleDialogActionPerformed(evt);
            }
        });

        jTable1SetScale.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Value", "Description"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1SetScale.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1SetScale.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(jTable1SetScale);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1OKSetScaleDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton1AddScaleValuePair, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton2CancelSetScaleDialog, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton2RemoveScaleValuePair, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1AddScaleValuePair)
                    .addComponent(jButton2RemoveScaleValuePair))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1OKSetScaleDialog)
                    .addComponent(jButton2CancelSetScaleDialog))
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog1SetScaleLayout = new javax.swing.GroupLayout(jDialog1SetScale.getContentPane());
        jDialog1SetScale.getContentPane().setLayout(jDialog1SetScaleLayout);
        jDialog1SetScaleLayout.setHorizontalGroup(
            jDialog1SetScaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1SetScaleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDialog1SetScaleLayout.setVerticalGroup(
            jDialog1SetScaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1SetScaleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDialog1SetUtilFn.setTitle("Set Utility Function");
        jDialog1SetUtilFn.setBounds(new java.awt.Rectangle(0, 0, 500, 350));
        jDialog1SetUtilFn.setMinimumSize(new java.awt.Dimension(100, 80));
        jDialog1SetUtilFn.setModal(true);

        jComboBox1UtilFnPickValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1UtilFnPickValueActionPerformed(evt);
            }
        });

        jTable1UtilFunction.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTable1UtilFunction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1UtilFunction.setColumnSelectionAllowed(true);
        jTable1UtilFunction.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1UtilFunction.getTableHeader().setReorderingAllowed(false);
        jTable1UtilFunction.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1UtilFunctionMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTable1UtilFunction);

        jButton1OKSetUtilFnDialog.setText("OK");
        jButton1OKSetUtilFnDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1OKSetUtilFnDialogActionPerformed(evt);
            }
        });

        jButton2CancelSetUtilFnDialog.setText("Cancel");
        jButton2CancelSetUtilFnDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2CancelSetUtilFnDialogActionPerformed(evt);
            }
        });

        jLabel4UtilFnInfo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton1SetDefaultUtilFnValues.setText("Set Default Value");
        jButton1SetDefaultUtilFnValues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1SetDefaultUtilFnValuesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 695, Short.MAX_VALUE)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jButton1OKSetUtilFnDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2CancelSetUtilFnDialog, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel4UtilFnInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(jComboBox1UtilFnPickValue, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1SetDefaultUtilFnValues, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1UtilFnPickValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1SetDefaultUtilFnValues))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4UtilFnInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1OKSetUtilFnDialog)
                    .addComponent(jButton2CancelSetUtilFnDialog))
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog1SetUtilFnLayout = new javax.swing.GroupLayout(jDialog1SetUtilFn.getContentPane());
        jDialog1SetUtilFn.getContentPane().setLayout(jDialog1SetUtilFnLayout);
        jDialog1SetUtilFnLayout.setHorizontalGroup(
            jDialog1SetUtilFnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jDialog1SetUtilFnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDialog1SetUtilFnLayout.setVerticalGroup(
            jDialog1SetUtilFnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1SetUtilFnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jFileChooser1Open.setCurrentDirectory(new File("."));
        jFileChooser1Open.setDialogTitle("Open saved DEX file");
        jFileChooser1Open.setFileFilter(dexFilter);
        jFileChooser1Open.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1OpenActionPerformed(evt);
            }
        });

        jFileChooser1Save.setDialogType(javax.swing.JFileChooser.SAVE_DIALOG);
        jFileChooser1Save.setCurrentDirectory(new File("."));
        jFileChooser1Save.setDialogTitle("Save DEX file");
        jFileChooser1Save.setFileFilter(dexFilter);
        jFileChooser1Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1SaveActionPerformed(evt);
            }
        });
        jFileChooser1Save.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jFileChooser1SavePropertyChange(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("DEX Method");
        setName("mainFrame"); // NOI18N

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTabbedPane1.setToolTipText("");
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        jTree2.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTree2.setScrollsOnExpand(false);
        jTree2.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTree2ValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jTree2);

        jButton1Add.setText("Add");
        jButton1Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1AddActionPerformed(evt);
            }
        });

        jButton2Remove.setText("Remove");
        jButton2Remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2RemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton2Remove, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(jButton1Add, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jButton1Add)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2Remove)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel7Attribute.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Attribute"));

        jLabel1.setText("Name");

        jLabel2.setText("Description");

        jTextField1AttName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1AttNameActionPerformed(evt);
            }
        });
        jTextField1AttName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1AttNameKeyTyped(evt);
            }
        });

        jTextField2AttDesc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2AttDescActionPerformed(evt);
            }
        });
        jTextField2AttDesc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField2AttDescKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel7AttributeLayout = new javax.swing.GroupLayout(jPanel7Attribute);
        jPanel7Attribute.setLayout(jPanel7AttributeLayout);
        jPanel7AttributeLayout.setHorizontalGroup(
            jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7AttributeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(27, 27, 27)
                .addGroup(jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1AttName)
                    .addComponent(jTextField2AttDesc))
                .addContainerGap())
        );
        jPanel7AttributeLayout.setVerticalGroup(
            jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7AttributeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1AttName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7AttributeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2AttDesc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7Scale.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Scale"));

        jButton1SetScale.setText("Set Scale");
        jButton1SetScale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1SetScaleActionPerformed(evt);
            }
        });

        jComboBox1ScaleNames.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ScaleNamesActionPerformed(evt);
            }
        });

        jButton1ScaleRemove.setText("Remove Scale");
        jButton1ScaleRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ScaleRemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7ScaleLayout = new javax.swing.GroupLayout(jPanel7Scale);
        jPanel7Scale.setLayout(jPanel7ScaleLayout);
        jPanel7ScaleLayout.setHorizontalGroup(
            jPanel7ScaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7ScaleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1SetScale, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1ScaleRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1ScaleNames, 0, 202, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7ScaleLayout.setVerticalGroup(
            jPanel7ScaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7ScaleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7ScaleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1SetScale)
                    .addComponent(jComboBox1ScaleNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1ScaleRemove))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7UtilityFunction.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Utility Function"));
        jPanel7UtilityFunction.setName("Utility Function"); // NOI18N

        jButton1SetUtilityFunction.setText("Set Utility Function");
        jButton1SetUtilityFunction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1SetUtilityFunctionActionPerformed(evt);
            }
        });

        jTextField3UtilityFunction.setEditable(false);

        javax.swing.GroupLayout jPanel7UtilityFunctionLayout = new javax.swing.GroupLayout(jPanel7UtilityFunction);
        jPanel7UtilityFunction.setLayout(jPanel7UtilityFunctionLayout);
        jPanel7UtilityFunctionLayout.setHorizontalGroup(
            jPanel7UtilityFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7UtilityFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1SetUtilityFunction)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField3UtilityFunction)
                .addContainerGap())
        );
        jPanel7UtilityFunctionLayout.setVerticalGroup(
            jPanel7UtilityFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7UtilityFunctionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7UtilityFunctionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1SetUtilityFunction)
                    .addComponent(jTextField3UtilityFunction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTextArea1TEST.setColumns(20);
        jTextArea1TEST.setRows(5);
        jScrollPane1.setViewportView(jTextArea1TEST);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel7Attribute, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7Scale, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7UtilityFunction, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7Attribute, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7Scale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7UtilityFunction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Decision Model", jPanel1);

        jTable1Alternatives.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTable1Alternatives.setColumnSelectionAllowed(true);
        jTable1Alternatives.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1Alternatives.getTableHeader().setReorderingAllowed(false);
        jTable1Alternatives.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1AlternativesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable1Alternatives);

        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton1AddAlternative.setText("Add Alternative");
        jButton1AddAlternative.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1AddAlternativeActionPerformed(evt);
            }
        });

        jButton1RemoveAlternative.setText("Remove Alternative");
        jButton1RemoveAlternative.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1RemoveAlternativeActionPerformed(evt);
            }
        });

        jComboBox2AttributeScaleValues.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2AttributeScaleValuesActionPerformed(evt);
            }
        });

        jTextField1AlternativeRename.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1AlternativeRenameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1AlternativeRename, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jComboBox2AttributeScaleValues, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addComponent(jButton1AddAlternative, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1RemoveAlternative, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1AddAlternative)
                    .addComponent(jButton1RemoveAlternative)
                    .addComponent(jComboBox2AttributeScaleValues, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1AlternativeRename, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 826, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Alternatives", jPanel2);

        jTable1Evaluation.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTable1Evaluation.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable1Evaluation.setColumnSelectionAllowed(true);
        jTable1Evaluation.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1Evaluation.getTableHeader().setReorderingAllowed(false);
        jTable1Evaluation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1EvaluationMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTable1Evaluation);

        jPanel10.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jButton1Reevaluate.setText("Reevaluate");
        jButton1Reevaluate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ReevaluateActionPerformed(evt);
            }
        });

        jLabel4Info1.setMaximumSize(new java.awt.Dimension(35, 23));
        jLabel4Info1.setMinimumSize(new java.awt.Dimension(35, 23));
        jLabel4Info1.setPreferredSize(new java.awt.Dimension(35, 23));

        jLabel5Info2.setMaximumSize(new java.awt.Dimension(35, 23));
        jLabel5Info2.setMinimumSize(new java.awt.Dimension(35, 23));
        jLabel5Info2.setPreferredSize(new java.awt.Dimension(35, 23));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1Reevaluate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4Info1, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5Info2, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1Reevaluate)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jLabel4Info1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel5Info2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 305, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Evaluation Results", jPanel3);

        jLabel3Chart.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3Chart.setText("CHART");

        javax.swing.GroupLayout jPanel4ChartLayout = new javax.swing.GroupLayout(jPanel4Chart);
        jPanel4Chart.setLayout(jPanel4ChartLayout);
        jPanel4ChartLayout.setHorizontalGroup(
            jPanel4ChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4ChartLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3Chart, javax.swing.GroupLayout.DEFAULT_SIZE, 826, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4ChartLayout.setVerticalGroup(
            jPanel4ChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4ChartLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3Chart, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Charts", jPanel4Chart);

        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 851, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        jLabel4StatusBarLabel.setText("Status Bar");

        fileMenu.setMnemonic('f');
        fileMenu.setText("File");

        newMenuItem.setMnemonic('n');
        newMenuItem.setText("New");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newMenuItem);

        openMenuItem.setMnemonic('o');
        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        saveMenuItem.setMnemonic('s');
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('a');
        saveAsMenuItem.setText("Save As ...");
        saveAsMenuItem.setDisplayedMnemonicIndex(5);
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        exitMenuItem.setMnemonic('x');
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setMnemonic('e');
        editMenu.setText("Edit");

        cutMenuItem.setMnemonic('t');
        cutMenuItem.setText("Cut");
        editMenu.add(cutMenuItem);

        copyMenuItem.setMnemonic('y');
        copyMenuItem.setText("Copy");
        editMenu.add(copyMenuItem);

        pasteMenuItem.setMnemonic('p');
        pasteMenuItem.setText("Paste");
        editMenu.add(pasteMenuItem);

        deleteMenuItem.setMnemonic('d');
        deleteMenuItem.setText("Delete");
        editMenu.add(deleteMenuItem);

        menuBar.add(editMenu);

        helpMenu.setMnemonic('h');
        helpMenu.setText("Help");

        contentsMenuItem.setMnemonic('c');
        contentsMenuItem.setText("Contents");
        helpMenu.add(contentsMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText("About");
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel4StatusBarLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 807, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4StatusBarLabel)
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(434, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void jButton1AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1AddActionPerformed
        // TODO add your handling code here:
        int attIdCounter = ogrodje.getwModel().getNodeCounter();
        Attribute a = new Attribute();
        a.setId(attIdCounter);
        a.setName("New Attribute "+attIdCounter);
        ogrodje.getwModel().setNodeCounter(attIdCounter + 1);
        addObject(a);
        expandAllNodes(0, jTree2.getRowCount());
        
        //PREVERI VSE ALTERNATIVE IN JIM DODAJ nov entry za nov atribut z default value .
        Alternative alt = null;
        Map.Entry<Attribute, String> entry = null;
        for (int i=0; i<ogrodje.getwModel().getAlternatives().size(); i++) {
            int stAtts = ogrodje.getwModel().getAttributes().size();
            alt = ogrodje.getwModel().getAlternatives().get(i);
            
            if (alt.getValues().size() < stAtts) {
                System.out.println("Alternativi alt="+alt.getName()+" bom dodal default entry za nov Atribut.-----"+"stAtts="+stAtts+" alt.getValues.size="+alt.getValues().size());
                entry = new AbstractMap.SimpleEntry<>(a, ".");
                alt.getValues().add(entry);
                System.out.println("Alternativi alt="+alt.getName()+" dodal default entry za nov Atribut.-----"+"stAtts="+stAtts+" alt.getValues.size="+alt.getValues().size());
            }
            
        }
        
        jLabel4StatusBarLabel.setText("<html>" + a.getName() + " added" + "</html>");
        refreshAttributes();
    }//GEN-LAST:event_jButton1AddActionPerformed

    private void jButton2RemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2RemoveActionPerformed
        // TODO add your handling code here:
        removeCurrentNode();
        expandAllNodes(0, jTree2.getRowCount());
        refreshAttributes();
    }//GEN-LAST:event_jButton2RemoveActionPerformed

    private void jTextField1AttNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1AttNameActionPerformed
        // TODO add your handling code here:
        //preveri ali atribut obstaja, če ja ga uredi, če ne ga ustvari in uredi
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        String name;
        Attribute a;
        if (selectedNode != null) {
            if (selectedNode.getUserObject() == null) {
                name = jTextField1AttName.getText();
                a = new Attribute();
                a.setName(name);
                //določi ID, najdi starša in določi ID,..
                //selectedNode.getParent();

                a.setId(0);
                a.setId_parent(0);

                a.setId_scale(0);
                a.setDescription("");
                selectedNode.setUserObject(a);
            } else {
                name = jTextField1AttName.getText();
                a = (Attribute) selectedNode.getUserObject();
                for (int i=0; i<ogrodje.getwModel().getAlternatives().size(); i++) {
                    for (int j=0; j<ogrodje.getwModel().getAlternatives().get(i).getValues().size(); j++) {
                        if (ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).getKey().getId() == a.getId()) {
                            ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).getKey().setName(name);
                        }
                    }
                }
                a.setName(name);
                selectedNode.setUserObject(a);
                for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
                    if (ogrodje.getwModel().getAttributes().get(i).getId() == a.getId()) {
                        ogrodje.getwModel().getAttributes().set(i, a);
                    }
                }
                
            }
            int [] rows = jTree2.getSelectionRows();
            ((DefaultTreeModel)jTree2.getModel()).reload();
            expandAllNodes(0, jTree2.getRowCount());
            jTree2.setSelectionRows(rows);
        }
        System.out.println("jTextField1AttNameActionPerformed");
    }//GEN-LAST:event_jTextField1AttNameActionPerformed
    
    /**Uporabi za prikaz vrednosti atributa glede na selekcijo v drevesu*/
    private void jTree2ValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTree2ValueChanged
        // TODO add your handling code here:
        JTree tree = (JTree) evt.getSource();
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        if (selectedNode != null) {
            if (selectedNode.getUserObject() != null) {
                //System.out.println("NOT V IFu - izpisat se more Name in Description");
                Attribute a = (Attribute) selectedNode.getUserObject();
                jTextField1AttName.setText(a.getName());
                jTextField2AttDesc.setText(a.getDescription());
                int idScale = a.getId_scale();
                if (idScale != -1) {
                    for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
                        if (ogrodje.getwModel().getScales().get(i).getId() == idScale) {
                            jComboBox1ScaleNames.setSelectedItem(ogrodje.getwModel().getScales().get(i));
                        }
                    }
                } else {
                    jComboBox1ScaleNames.setSelectedIndex(0);
                }
                jLabel4StatusBarLabel.setText("<html>" + ((Attribute)selectedNode.getUserObject()).getName() + "</html>");
            }
            if (selectedNode.isLeaf()) {
                jPanel7UtilityFunction.setVisible(false);
            } else {
                Attribute a = (Attribute) selectedNode.getUserObject();
                boolean check = preveriAliImajoVsiOtrociAtributaNastavljenScale(a);
                jPanel7UtilityFunction.setVisible(check);
                //tukaj pa pride izpisovanje opisa utility function ter imena utility function
                if (check) {
                    for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
                        if (a.getId() == ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute()) {
                            jTextField3UtilityFunction.setText(ogrodje.getwModel().getUtilFunctions().get(i).getDescription());
                            break;
                        } else {
                            jTextField3UtilityFunction.setText("No utility function set.");
                        }
                    }
                }
            }
        }
    }//GEN-LAST:event_jTree2ValueChanged

    private boolean preveriAliImajoVsiOtrociAtributaNastavljenScale(Attribute a) {
        if (a.getId_scale() == -1) {
            return false;
        }
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            if (a.getId() == ogrodje.getwModel().getAttributes().get(i).getId_parent()) {
                if (ogrodje.getwModel().getAttributes().get(i).getId_scale() != -1) {
                    //ok
                } else {
                    return false;
                }
            }
        }
        return true;
    }
    
    private void jTextField2AttDescActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2AttDescActionPerformed
        // TODO add your handling code here:
        //preveri ali atribut obstaja, če ja ga uredi, če ne ga ustvari in uredi
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        String desc;
        Attribute a;
        if (selectedNode != null) {
            if (selectedNode.getUserObject() == null) {
                desc = jTextField2AttDesc.getText();
                a = new Attribute();
                a.setDescription(desc);
                //določi ID, najdi starša in določi ID,..
                //selectedNode.getParent();

                a.setId(0);
                a.setId_parent(0);

                a.setId_scale(0);
                a.setName("");
                selectedNode.setUserObject(a);
            } else {
                desc = jTextField2AttDesc.getText();
                a = (Attribute) selectedNode.getUserObject();
                for (int i=0; i<ogrodje.getwModel().getAlternatives().size(); i++) {
                    for (int j=0; j<ogrodje.getwModel().getAlternatives().get(i).getValues().size(); j++) {
                        if (ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).getKey().getId() == a.getId()) {
                            ogrodje.getwModel().getAlternatives().get(i).getValues().get(j).getKey().setDescription(desc);
                        }
                    }
                }
                a.setDescription(desc);
                selectedNode.setUserObject(a);
                for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
                    if (ogrodje.getwModel().getAttributes().get(i).getId() == a.getId()) {
                        ogrodje.getwModel().getAttributes().set(i, a);
                    }
                }
            }
            int [] rows = jTree2.getSelectionRows();
            ((DefaultTreeModel)jTree2.getModel()).reload();
            expandAllNodes(0, jTree2.getRowCount());
            jTree2.setSelectionRows(rows);
        }
        System.out.println("jTextField2AttDescActionPerformed");
    }//GEN-LAST:event_jTextField2AttDescActionPerformed

    private void jComboBox1ScaleNamesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ScaleNamesActionPerformed
        // TODO add your handling code here:
        //če je izbran prvi Scale potem se ga ignorira, vsi ostali so zares
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a;
        if (selectedNode != null) {
            a = (Attribute) selectedNode.getUserObject();
            if (jComboBox1ScaleNames.getSelectedIndex() != 0) {
//                Scale s = (Scale) jComboBox1ScaleNames.getSelectedItem();
//                for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
//                    if (s.getId() == ogrodje.getwModel().getScales().get(i).getId()) {
//                        a.setId_scale(s.getId());
//                        for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
//                            if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
//                                ogrodje.getwModel().getAttributes().set(j, a);
//                                selectedNode.setUserObject(a);
//                            }
//                        }
//                        //tukaj preleti še alternative in v vsaki vsakemu atributu, ki se ujema prilagodi id_scale
//                        refreshScaleIDtoAllAttributesInAllAlternatives(s.getId(), a);
//                    }
//                }
            } else {    //nastavim id_scale nazaj na -1
                a.setId_scale(-1);
                for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
                    if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
                        ogrodje.getwModel().getAttributes().set(j, a);
                        selectedNode.setUserObject(a);
                    }
                }
                //tukaj preleti še alternative in v vsaki vsakemu atributu, ki se ujema prilagodi id_scale
                refreshScaleIDtoAllAttributesInAllAlternatives(-1, a);
            }
        }
        refreshAttributes();
    }//GEN-LAST:event_jComboBox1ScaleNamesActionPerformed

    
    private void jButton1SetScaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1SetScaleActionPerformed
        // TODO add your handling code here:
        /*Preveri ali je izbran atribut ter če je odpri jDialog ter omogoči spreminjanje Scale*/
        jTable1SetScale.setModel(createMyScaleTableModel());
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a;
        if (selectedNode != null) {
            a = (Attribute) selectedNode.getUserObject();
            
            if (jComboBox1ScaleNames.getSelectedIndex() != 0) {
                Scale s = (Scale) jComboBox1ScaleNames.getSelectedItem();
                
                //ČE JE SCALE V COMBOBOXU DRUGAČEN OD TRENUTNO DOLOČENEGA GA SAMO ZAMENJAJ IN NE ODPIRAJ DIALOGA
                if (s.getId() != a.getId_scale()) {
                    System.out.println("TU NOT! ko je ID scale pri atributu drugačen od scala izbranega v comboboxu");
                    a.setId_scale(s.getId());
                    for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
                        if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
                            ogrodje.getwModel().getAttributes().set(j, a);
                        }
                    }
                    jLabel4StatusBarLabel.setText("<html>" + s.getName() + " selected" + "</html>");
                    refreshScaleIDtoAllAttributesInAllAlternatives(s.getId(), a);
                    selectedNode.setUserObject(a);
                } else {
                    jDialog1SetScale.setVisible(true);
                }
                
                /*
                //pridobi izbrani Scale in ga dodeli atributu ter shrani
                for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
                    if (s.getId() == ogrodje.getwModel().getScales().get(i).getId()) {
                        a.setId_scale(s.getId());
                        for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
                            if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
                                ogrodje.getwModel().getAttributes().set(j, a);
                                selectedNode.setUserObject(a);
                                //tu potrebno preiti še vse atribute v alternativah in ažurirati spremembo? dajmo to raje ko se v dialogu potrdi.
                            }
                        }
                    }
                }*/
                
            } else {
                //drugače prikaži dialog
                jDialog1SetScale.setVisible(true);
            }
            refreshAttributes();
        }
    }//GEN-LAST:event_jButton1SetScaleActionPerformed

    private void jButton1SetUtilityFunctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1SetUtilityFunctionActionPerformed
        // TODO add your handling code here:
        System.err.println("jButton1SetUtilityFunctionActionPerformed");
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        //selectedNode sigurno ni Leaf node in sigurno ima otroke ter vsi imajo nastavljen Scale
        
        Attribute a = (Attribute) selectedNode.getUserObject();
        ArrayList<Attribute> atts = ogrodje.getwModel().getAttributes();
        ArrayList<Attribute> utilFnAtts = new ArrayList<Attribute>();
        for (int i=0; i<atts.size(); i++) {
            if (atts.get(i).getId_parent() == a.getId()) {
                utilFnAtts.add(atts.get(i));
            }
        }
        jTable1UtilFunction.setModel(createMyUtilFnTableModel(a, utilFnAtts));
        jDialog1SetUtilFn.setVisible(true);
    }//GEN-LAST:event_jButton1SetUtilityFunctionActionPerformed

    private void jTextField1AttNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1AttNameKeyTyped
        // TODO add your handling code here:
        //ODVEČ KER SE OB PROŽENJU IZGUBI SELECTIONPATH
    }//GEN-LAST:event_jTextField1AttNameKeyTyped

    private void jTextField2AttDescKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2AttDescKeyTyped
        // TODO add your handling code here:
        //ODVEČ KER SE IZGUBI SELECTION
    }//GEN-LAST:event_jTextField2AttDescKeyTyped

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        // TODO add your handling code here:
        JTabbedPane sourceTabbedPane = (JTabbedPane) evt.getSource();
        int index = sourceTabbedPane.getSelectedIndex();
        System.out.println("Tab changed to: " + sourceTabbedPane.getTitleAt(index));
        
        switch (index) {
            case 0:
                
                break;
            case 1:
                jTable1Alternatives.setModel(createMyTableModel());
                break;
            case 2:
                jTable1Evaluation.setModel(createMyEvaluationTableModel());
                updateInfoLabels("Evaluate by pressing Reevaluate", "");
                break;
            case 3:     //tukaj preberem vrednosti iz Evaluation in ustvarim graf
                runTheEvaluationAndDrawChart();
                break;
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged
    
    private void runTheEvaluationAndDrawChart() {
        
        Evaluation e = ogrodje.getwModel().getE();
        boolean evaluated = true;
        String tOutput = "";
        for (int i=0; i<at.size(); i++) {
            tOutput = " | ";
            for (int j=0; j<al.size(); j++) {
                tOutput += e.t[i][j] + " | ";
                if (e.t[i][j].equals(".")) {
                    evaluated = false;
                }
            }
            System.out.println(tOutput);
        }
        //to zgoraj tak ni pomembno. Vglavnem kličem metodo za risanje grafa
        System.out.println("Evaluated = " + evaluated);
        if (evaluated) {

            Attribute pAtt = null;
            Scale pS = null;
            refreshReferencesToCollections();
            for (int i=0; i<at.size(); i++) {
                System.out.println("i="+i);
                if (at.get(i).getId_parent() == -1) {
                    pAtt = at.get(i);
                    System.out.println("pAtt.toString="+pAtt.toString());
                    for (int j=0; j<sc.size(); j++) {
                        System.out.println("j="+j);
                        if (pAtt.getId_scale() == sc.get(j).getId()) {
                            pS = sc.get(j);
//                            stVrednosti = sc.get(i).getScaleValuePairs().size();
                            System.out.println("pS.toString="+pS.toString());
                            break;
                        }
                    }
                    break;
                }
            }

            try {
                File chart = drawBarChart(pAtt, pS);
                jLabel3Chart.setIcon(barChartIcon);
            } catch (IOException ex) {
                updateInfoLabels("Error drawing chart", "Successfully run the evaluation first");
                Logger.getLogger(DEXMethodApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
            jLabel4StatusBarLabel.setText("Chart successfully drawn and saved to chart.png");
            jLabel3Chart.setText("");
        } else {
            jLabel3Chart.setIcon(null);
            jLabel3Chart.repaint();
            jLabel3Chart.setText("Error drawing chart");
            jLabel4StatusBarLabel.setText("Error drawing chart! Successfully run the evaluation first!");
        }
    }
    
    /**Izpišem graf končne evaluacije.
     * Prikažem Root Attribute (ID = -1), njegove Scale Values na lestvici na levi,
     * ter vse alternative in njihove vrednosti.
     */
    private File drawBarChart(Attribute pAtt, Scale pS) throws IOException {
        System.out.println("drawBarChart pAtt="+pAtt.toString());
        System.out.println("drawBarChart pS="+pS.toString());
        
        File barChartPNG = new File("chart.png");
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        
        for (int i=0; i<al.size(); i++) {
            for (int j=0; j<al.get(i).getValues().size(); j++) {
                if (al.get(i).getValues().get(j).getKey().getId() == pAtt.getId()) {
                    //našel pravi atribut (root) v trenutni alternativi, sedaj potrebujem št vrednosti med pari vrednosti
                    
                    for (int k=0; k<pS.getScaleValuePairs().size(); k++) {
                        if (al.get(i).getValues().get(j).getValue().equals(pS.getScaleValuePairs().get(k).getName())) {
                            dataset.setValue(k, "Alternatives", al.get(i).getName());
                        }
                    }
                    
                }
            }
        }
        System.out.println("ColumnCount="+dataset.getColumnCount());
        System.out.println("ColumnKeysSize="+dataset.getColumnKeys().size());
        System.out.println("RowCount="+dataset.getRowCount());
        System.out.println("RowKeysSize="+dataset.getRowKeys().size());
        
        JFreeChart barChart = ChartFactory.createBarChart("Evaluated Alternatives",
                        "Alternatives", 
                        "Scale Values", 
                        dataset, PlotOrientation.VERTICAL, 
                        false, true, false);
        
        String[] values = new String[pS.getScaleValuePairs().size()];
        for (int i=0; i<pS.getScaleValuePairs().size(); i++) {
            values[i] = pS.getScaleValuePairs().get(i).getName();
        }
        SymbolAxis rangeAxis = new SymbolAxis("Scale Values", values);
        rangeAxis.setTickUnit(new NumberTickUnit(1));
        rangeAxis.setAutoRange(false);
        rangeAxis.setRange(0, values.length);
        rangeAxis.setVisible(true);
        barChart.getCategoryPlot().setRangeAxis(rangeAxis);
        
        CategoryPlot cp = barChart.getCategoryPlot();
        BarRenderer sbr = (BarRenderer) cp.getRenderer();
        sbr.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{1}", new DecimalFormat("0")));
        sbr.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.TOP_CENTER));
        sbr.setBaseItemLabelsVisible(true);
        
        ChartUtilities.saveChartAsPNG(barChartPNG, barChart, 826, 363);

        if (barChartIcon != null) {
            barChartIcon.getImage().flush();
        }
        barChartIcon = new ImageIcon(barChartPNG.getAbsolutePath());

        return barChartPNG;
    }
    
    private void jButton1AddAlternativeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1AddAlternativeActionPerformed
        // TODO add your handling code here:
        //v tabeli na desni dodaj še en stolpec
        int altIdCounter = ogrodje.getwModel().getAlternativeCounter();
        Alternative alt = new Alternative();
        Attribute att = null;
        alt.setId(altIdCounter);
        alt.setName("Alternative "+altIdCounter);
        ogrodje.getwModel().setAlternativeCounter(altIdCounter + 1);
        String attName, scaleValue;
        int idScale;
        for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
            att = ogrodje.getwModel().getAttributes().get(i);
            idScale = att.getId_scale();
            scaleValue = ".";
            for (int j=0; j<ogrodje.getwModel().getScales().size(); j++) {
                if (idScale == ogrodje.getwModel().getScales().get(j).getId()) {
                    //v pravem Scalu, sedaj preletim še vrednosti in jih vpisujem
                    //for (int k=0; k<ogrodje.getwModel().getScales().get(j).getScaleValuePairs().size(); k++) {
                        //morda potrebno ScalePairom dodati id, in poiskati pravo obstoječo vrednost
                        
                        //scaleValue = ogrodje.getwModel().getScales().get(j).getScaleValuePairs().get(0).getName();      //vzamem prvo vrednost
                        scaleValue = ".";               //PIKA!
                    //}
                }
            }
            Map.Entry<Attribute, String> entry = new AbstractMap.SimpleEntry<>(att, scaleValue);
            alt.getValues().add(entry);    //att name in scalepair value
            //System.out.println("KAKO NARAŠČA alt.getValues.size? = "+alt.getValues().size()); OK!
        }
        jLabel4StatusBarLabel.setText("<html>" + "Alternative " + alt.getName() + " added" + "</html>");
        ogrodje.getwModel().getAlternatives().add(alt);
        jTable1Alternatives.setModel(createMyTableModel());
    }//GEN-LAST:event_jButton1AddAlternativeActionPerformed

    private void jButton1RemoveAlternativeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1RemoveAlternativeActionPerformed
        // TODO add your handling code here:
        int selectedColumn = jTable1Alternatives.getSelectedColumn();
        if (selectedColumn != 0 && selectedColumn != -1) {
            String altName = jTable1Alternatives.getValueAt(0, selectedColumn).toString();
            for (int i=0; i<ogrodje.getwModel().getAlternatives().size(); i++) {
                if (altName.equals(ogrodje.getwModel().getAlternatives().get(i).getName())) {
                    ogrodje.getwModel().getAlternatives().remove(ogrodje.getwModel().getAlternatives().get(i));
                }
            }
            jLabel4StatusBarLabel.setText("<html>" + "Alternative " + altName + " removed" + "</html>");
            jTable1Alternatives.setModel(createMyTableModel());
        }
    }//GEN-LAST:event_jButton1RemoveAlternativeActionPerformed

    private void jComboBox2AttributeScaleValuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2AttributeScaleValuesActionPerformed
        // TODO add your handling code here:
        //najdi alternativo in ustrezni atribut, v comboboxu so izpisane možne vrednosti scale, ob proženju pa se izbira shrani k atributu
        Alternative alt = null;
        Attribute att = null;
        int col = jTable1Alternatives.getSelectedColumn();
        int row = jTable1Alternatives.getSelectedRow();
        if (row != -1 && row != 0 && col != -1 && col != 0) {
            int index = jComboBox2AttributeScaleValues.getSelectedIndex();
            ScalePair sp = (ScalePair) jComboBox2AttributeScaleValues.getSelectedItem();
            //System.out.println("V IFu pred pravim IFu  index="+index+"  sp.toString="+sp.toString());
            
            if ((sp.getName().equals("Invalid Selection") || sp.getName().equals("Set Scale To The Attribute First")) && index == 0) {
                //nič ne nastavljam ker ni kaj
            } else {
                //System.out.println("V pravem IFu za spremembo vrednosti glede na ComboBox");
                //poiščem ustrezni Atribut, ustrezno Alternativo, in 
                alt = ogrodje.getwModel().getAlternatives().get(col - 1);
                att = ogrodje.getwModel().getAttributes().get(row - 1);
                //LinkedList<Map.Entry<Attribute, String>> newValuesList = alt.getValues();
                Map.Entry<Attribute, String> entry = null;
                for (int i=0; i<alt.getValues().size(); i++) {
                    if (alt.getValues().get(i).getKey().getId() == att.getId()) {
                        entry = alt.getValues().get(i);
                        entry.setValue(sp.getName());
                        alt.getValues().set(i, entry);
                        
                        System.out.println("----entry: key="+entry.getKey().toString()+" value="+entry.getValue());
                        System.out.println("----alt.getValues.get(i): key="+alt.getValues().get(i).getKey().toString()+ " value="+alt.getValues().get(i).getValue());
                        jTable1Alternatives.getModel().setValueAt(alt.getValues().get(i).getValue()/*entry.getValue()*/, row, col);
                        
                        ogrodje.getwModel().getAlternatives().get(col - 1).getValues().set(i, entry);       //V TEJ FUNKCIJI LAHKO PREVERJAM ALI DELUJE PO REFERENCI ALI KAKO?
                        //NAMREČ NAJPREJ SEM SPREMENIL VREDNOST V spremenljivki alt, sedaj pa tudi v GLAVNI ZBIRKI
                    }
                }
                
                //newValuesList.set(index, entry)
                //System.out.println("PRED SPREMEMBO: att.toString="+entry.getKey()+ " value="+entry.getValue());
                //System.out.println("----alt.getValues.get(index): key="+alt.getValues().get(index).getKey().toString()+ " value="+alt.getValues().get(index).getValue());
                
                //System.out.println("MED SPREMEMBO: att.toString="+entry.getKey()+ " value="+entry.getValue());
                
                //System.out.println("PO SPREMEMBI: alt.getValues.get(index) index="+index+" KEY="+alt.getValues().get(index).getKey()+ " VALUE="+alt.getValues().get(index).getValue());
                        //ažuriram vrednost
                        //osvežim v zbirki
                        //ponovno prikažem tabelo in izberem enake vrednosti
                //jTable1Alternatives.setModel(createMyTableModel());
                //jComboBox2AttributeScaleValues.setSelectedIndex(index);
            }
        }
    }//GEN-LAST:event_jComboBox2AttributeScaleValuesActionPerformed

    private void jButton1AddScaleValuePairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1AddScaleValuePairActionPerformed
        // TODO add your handling code here:
        //USTVARI NOV ValuePair
        myScaleTableModel.getRowCount();
        myScaleTableModel.addRow(new Object [] { ("Value " + (myScaleTableModel.getRowCount() - 1)), "" });
    }//GEN-LAST:event_jButton1AddScaleValuePairActionPerformed

    private void jButton2RemoveScaleValuePairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2RemoveScaleValuePairActionPerformed
        // TODO add your handling code here:
        //ODSTRANI VALUEPAIR glede na izbrano vrstico
        int stolpec = jTable1SetScale.getSelectedColumn();
        int vrstica = jTable1SetScale.getSelectedRow();
        if (vrstica != -1 && vrstica != 0)
            myScaleTableModel.removeRow(vrstica);
    }//GEN-LAST:event_jButton2RemoveScaleValuePairActionPerformed

    private void jButton2CancelSetScaleDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2CancelSetScaleDialogActionPerformed
        // TODO add your handling code here:
        //KO SE DIALOG ZAPRE se pač zapre
        jDialog1SetScale.setVisible(false);
    }//GEN-LAST:event_jButton2CancelSetScaleDialogActionPerformed

    private void jButton1OKSetScaleDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1OKSetScaleDialogActionPerformed
        // TODO add your handling code here:
        //ko se dialog zapre se nov scale shrani izbranemu atributu
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        String name, desc;
        Attribute a;
        Scale s = null;
        ScalePair sp;
        if (selectedNode != null) {
            a = (Attribute) selectedNode.getUserObject();
            if (a.getId_scale() != -1) {
                for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
                    if (a.getId_scale() == ogrodje.getwModel().getScales().get(i).getId()) {
                        //editaj Scale
                        s = ogrodje.getwModel().getScales().get(i);

                        int stVrstic = myScaleTableModel.getRowCount() - 1; //Title vrstice ne štejemo
                        int stParov = s.getScaleValuePairs().size();
                        while (stVrstic != stParov) {
                            if (stVrstic > stParov) {
                                sp = new ScalePair();
                                s.getScaleValuePairs().add(sp);       //MOŽNO JE PUŠNITI NA MESTO
                                stParov++;
                            }
                            if (stVrstic < stParov) {
                                s.getScaleValuePairs().removeLast();
                                stParov--;
                            }
                        }

                        for (int j=0; j<stVrstic; j++) {
                            name = myScaleTableModel.getValueAt(j+1, 0).toString();
                            desc = myScaleTableModel.getValueAt(j+1, 1).toString();
                            //če je bila narejena razlika, je potrebno poiskati vse alternative, ki vsebujejo atribute s tem scalom in razveljaviti/preimenovati value
                            s.getScaleValuePairs().get(j).setName(name);
                            s.getScaleValuePairs().get(j).setDescription(desc);
                        }
                        
                        //--------------------
                        //preleti vse alternative in v values spremeni default value atributu s tem novim scalom
                        Alternative alt = null;
                        Map.Entry<Attribute, String> entry = null;
                        for (int k=0; k<ogrodje.getwModel().getAlternatives().size(); k++) {
                            alt = ogrodje.getwModel().getAlternatives().get(k);
                            for (int j=0; j<alt.getValues().size(); j++) {
                                entry = alt.getValues().get(j);     //Attribute, String(default/chosen ScalePair value, ki pripada določenemu Scalu)
                                if (entry.getKey().getId() == a.getId()) {
                                    entry.getKey().setId_scale(s.getId());  //vsakemu Atributu v vsaki Alternativi, se ažurira id_scale
                                    if (entry.getKey().getId_scale() == s.getId()) {
                                        //String newDefaultScaleValue = "";
                                        //poišči pravi scale in prilagodi vrednost! AJA TO JE S!
                                        //entry.setValue(s.getScaleValuePairs().get(0).getName());    //LAHKO BI DAL TUDI PIKO? NE.
                                        entry.setValue(".");        //PIKA!
                                        //pri evalvaciji se pač preveri da noben izmed atributov nima nastavljene pike. vsem ki pa imajo otroke se pri evalvaciji določi
                                        ogrodje.getwModel().getAlternatives().get(k).getValues().set(j, entry);     //NASTAVIM PRILAGOJEN ENTRY!
                                    }
                                }
                            }
                        }
                        //-------------------
                        
                        //SHRANIM S NAZAJ V SEZNAM, A V TREE in V SEZNAM!
                        ogrodje.getwModel().getScales().set(i, s);
                        a.setId_scale(s.getId());
                        for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
                            if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
                                ogrodje.getwModel().getAttributes().set(j, a);
                            }
                        }
                        selectedNode.setUserObject(a);      //verjetno odveč. kako se berejo atributi?
                        
                    } else {
                        //nič
                    }
                }
            } else {
                if (s == null) {        //če Scale še ne obstaja / ni bil najden zgoraj
                    //ustvari nov Scale
                    int scaleIdCounter = ogrodje.getwModel().getScaleCounter();
                    s = new Scale();
                    s.setId(scaleIdCounter);
                    ogrodje.getwModel().setScaleCounter(scaleIdCounter + 1);
                    int stVrstic = myScaleTableModel.getRowCount() - 1;
                    for (int i=0; i<stVrstic; i++) {
                        sp = new ScalePair();
                        s.getScaleValuePairs().add(sp);
                        name = myScaleTableModel.getValueAt(i+1, 0).toString();
                        desc = myScaleTableModel.getValueAt(i+1, 1).toString();
                        
                        s.getScaleValuePairs().get(i).setName(name);
                        s.getScaleValuePairs().get(i).setDescription(desc);
                    }

                    ogrodje.getwModel().getScales().add(s);
                    a.setId_scale(s.getId());
                    for (int i=0; i<ogrodje.getwModel().getAttributes().size(); i++) {
                        if (a.getId() == ogrodje.getwModel().getAttributes().get(i).getId()) {
                            ogrodje.getwModel().getAttributes().set(i, a);
                        }
                    }
                    selectedNode.setUserObject(a);      //verjetno odveč. kako se berejo atributi?
                    //jComboBox1ScaleNames.setModel(createMyComboBoxScaleModel());
                    //jComboBox1ScaleNames.setSelectedIndex(ogrodje.getwModel().getScales().size()+1);
                    
                    //preleti vse alternative in v values spremeni default value atributu s tem novim scalom
                    Alternative alt = null;
                    Map.Entry<Attribute, String> entry = null;
                    for (int i=0; i<ogrodje.getwModel().getAlternatives().size(); i++) {
                        alt = ogrodje.getwModel().getAlternatives().get(i);
                        for (int j=0; j<alt.getValues().size(); j++) {
                            entry = alt.getValues().get(j);
                            if (entry.getKey().getId() == a.getId()) {
                                entry.getKey().setId_scale(a.getId_scale());        //prilagodim ID Scale tudi entryju v seznamu v Alternativi
                                //String newDefaultScaleValue = "";
                                //poišči pravi scale in prilagodi vrednost! AJA TO JE S!
                                //entry.setValue(s.getScaleValuePairs().get(0).getName());    //LAHKO BI DAL TUDI PIKO? NE.
                                entry.setValue(".");    //LAHKO BI DAL TUDI PIKO? JA.
                                ogrodje.getwModel().getAlternatives().get(i).getValues().set(j, entry);     //NASTAVIM PRILAGOJEN ENTRY!
                            }
                        }
                    }
                }
            }
        }
        jComboBox1ScaleNames.setModel(createMyComboBoxScaleModel());
        jComboBox1ScaleNames.setSelectedItem(s);
        refreshAttributes();
        jDialog1SetScale.setVisible(false);
    }//GEN-LAST:event_jButton1OKSetScaleDialogActionPerformed

    private void jTable1AlternativesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1AlternativesMouseClicked
        // TODO add your handling code here:
        //ob vsakem kliku v tabelo alternativ odčitaj vrstico in stolpec ter ustvari nov model za list
        int row = jTable1Alternatives.rowAtPoint(evt.getPoint());
        int col = jTable1Alternatives.columnAtPoint(evt.getPoint());
        jTextField1AlternativeRename.setText("");
        if (row == 0) {     //morda pogoj da je v prvi vrstici ni potreben... ampak ok
            if (col > 0 && col <= al.size()) {
                for (int i=0; i<al.size(); i++) {
                    if (i == col-1) {
                        //našel alternativo ; prikaži ime
                        jTextField1AlternativeRename.setText(al.get(i).getName());
                    }
                }
            }
        }
        jComboBox2AttributeScaleValues.setModel(createMyComboBoxAlternativeScalePairsModel());
    }//GEN-LAST:event_jTable1AlternativesMouseClicked

    private void jTable1EvaluationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1EvaluationMouseClicked
        // TODO add your handling code here:
        int row = jTable1Alternatives.rowAtPoint(evt.getPoint());
        int col = jTable1Alternatives.columnAtPoint(evt.getPoint());
        //jComboBox2AttributeScaleValues.setModel(createMyComboBoxAlternativeScalePairsModel());
        //naredi kaj pač manjka, še kaj sploh? mislim da je vse ok :)
    }//GEN-LAST:event_jTable1EvaluationMouseClicked

    /**Preide po vrsti vsak atribut, ki ima utility function in prebere pogoj pri vsakem atributu
     * in glede na kombinacijo iz pripadajočega utility functiona odčita vrednost ter jo vpiše v Scale ter v tabelo
     *
     **/
    private boolean evaluirajOcetovskeAtributeGledeNaUtilityFunkcijeZaVsakoAlternativo() {
        boolean prekini = false;
        Attribute pAtt, att;
        Alternative alt;
        Scale s;
        UtilityFunction uF = null;
        String rezultat;
        String[][] tabela;
        int stevec = uf.size();     //ta števec se poveča vedno ko se ugotovi, da se očetovski atribut še ne da evaluirati
                                    //in zmanjša vedno, ko se ugotovi da je bil še neevaluiran atribut uspešno evaluiran.
        int stevecOtrok = 0;
        int idScale, idUf;
        ArrayList<Attribute> childAttributes = new ArrayList<Attribute>();
        ArrayList<Attribute> parentAttributes = new ArrayList<Attribute>();
        for (int i=0; i<at.size(); i++) {
            if (hasChildren(at.get(i))) {
                parentAttributes.add(at.get(i));            //MORDA JIH BO POTREBNO SORTIRATI PO NIVOJIH OD VEJ PROTI ROOT
            }
        }
        
        /* Grem skozi vsak očetovski atribut in za vsakega otroka poiščem UtilityFunction
        *   in glede na tabelo vrednosti poiščem tisto, ki se ujema, ter z rekurzijo ponavljam dokler
        *   ne pridem do prave evaluacije. Nato jo shranim v tabelo.
        */
        for (int x=0; x<parentAttributes.size(); x++) {
            prekini = false;
            pAtt = parentAttributes.get(x);
            childAttributes.clear();
            stevecOtrok = 0;
            //pridobim ustrezen utility function
            for (int y=0; y<uf.size(); y++) {
                if (uf.get(y).getId_attribute() == pAtt.getId()) {
                    uF = uf.get(y);
                    System.out.println("UTILITY FUNCTION "+ uF.getDescription() +"   |   values size="+uF.getValues().size());
//                    System.out.println(uF.outputValues());
                }
            }
            for (int i=0; i<at.size(); i++) {
                att = at.get(i);
                //če smo pri otroku starševskega atributa, ga dodam v seznam otrok, ki jih kasneje evaluiram
                if (pAtt.getId() == att.getId_parent()) {
                    childAttributes.add(att);
                    System.out.println("ChildAttributes: "+att.toString());
                } else {
                    //
                }
            }
            //izgradi tabelo ter glede na vrednost vsakega atributa poišči pravo vrednost v uF ter jo shrani v ustrezno alternativo
            tabela = izgradi2DArrayVrednostiZaEvaluacijo(pAtt, childAttributes);
//  ------------------ IZPIS TABELE
            for (int i=0; i<tabela.length; i++) {
                String vrsticaVTabeli = "  |  i="+i;
                for (int j=0; j<childAttributes.size(); j++) {
                    vrsticaVTabeli += "  |  "+tabela[i][j];
                }
                System.out.println(vrsticaVTabeli + "  |  " + uF.getValues().get(i) + "  |");
            }
//  ------------------ KONEC IZPISA TABELE
            
            String[] values = new String[childAttributes.size()];
            System.out.println("tabela.length="+tabela.length);
            for (int k=0; k<al.size(); k++) {
                alt = al.get(k);
                //tukaj kličem funkcijo ki bere 2Darray ter poišče ustrezno vrstico ter nato preleti utilityfunction
                //in v ujemajoči se vrstici v uF prebere odgovor ter ga shrani k očetovskemu atributu v ustrezno alternativo
                
                for (int j=0; j<childAttributes.size(); j++) {
                    //pridobim pravo iskano vrednost
                    for (int l=0; l<alt.getValues().size(); l++) {
                        if (childAttributes.get(j).getId() == alt.getValues().get(l).getKey().getId()) {
                            values[j] = alt.getValues().get(l).getValue();
                            System.out.println("values["+l+"]="+values[j]);
                            
                            //KRITIČNA TOČKA! ČE KATERA IZMED VALUES MANJKA JE POTREBNO PREKINITI VSE FOR ZANKE
                            //DO GLAVNE, PRED TEM PA parentAttribute odstraniti od začetka na konec seznama.
                            if (values[j].equals(".")) {
                                prekini = true;
                                
                                System.out.println("prekini == true;!!!!!!!!!!!! parentAttributes.size="+parentAttributes.size());
                                parentAttributes.remove(x);
                                System.out.println("removed pAtt    !!!!!!!!!!!! parentAttributes.size="+parentAttributes.size());
                                parentAttributes.add(pAtt);
                                System.out.println("added pAtt to end!!!!!!!!!!! parentAttributes.size="+parentAttributes.size());
                                x--;
                                break;
                            }
                        }
                        if (prekini)
                            break;
                    }
                    if (prekini)
                        break;
                }
                if (prekini)
                    break;
                
                //v vsaki vrstici se gre po vrsti skozi atribute in če se prvi atribut ujema s pravim se gre
                //gledat naprej drugega, tretjega,... dokler se ne ujemajo vsi. Takrat se prebere št vrstice in rezultat iz iste vrstice UF
                
                //skozi vsako vrstico. dokler se ne najde pravilna vrstica
                int pravilnaVrstica = -1;
                for (int l=0; l<tabela.length; l++) {

                    pravilnaVrstica = preidiAtributeInNajdiVrstico(l, 0, tabela, values, childAttributes);
                    
                    if (pravilnaVrstica != -1) {
                        System.out.println("vrstica="+l+" JE pravilnaVrstica! = "+pravilnaVrstica);
                        break;
                    }
                }
                
                rezultat = uF.getValues().get(pravilnaVrstica);
                System.out.println("Parent: "+ pAtt.getName() +"  |  Alternativa: "+ alt.getName() +"  |  rezultat="+rezultat);
                //vnos rezultata v alternativo na podlagi katere se nato izgradi model
                for (int l=0; l<alt.getValues().size(); l++) {
                    if (alt.getValues().get(l).getKey().getId() == pAtt.getId()) {
                        alt.getValues().get(l).setValue(rezultat);        //shranim evaluirano vrednost
                    }
                }
            }
        }
        
        System.out.println("evaluirajOcetovskeAtributeGledeNaUtilityFunkcijeZaVsakoAlternativo"+" IZVEDENA");
        return true;
    }
    
    private int preidiAtributeInNajdiVrstico(int trenutnaVrstica, int trenutniOtrok, String[][] tabela, String[] altValues, ArrayList<Attribute> childAttributes) {
        
        if (tabela[trenutnaVrstica][trenutniOtrok].equals(altValues[trenutniOtrok])) {
            //če se vrednost trenutne vrstice in atributa ujema z iskano vrednostjo za trenutni atribut
            //vrni št vrstice v kateri se najdejo prave iskane vrednosti
            // na podlagi te vrstice se lahko iz UF odčita iskana vrednost
            if (trenutniOtrok == childAttributes.size()-1) {
                return trenutnaVrstica;
            } else {
                return preidiAtributeInNajdiVrstico(trenutnaVrstica, trenutniOtrok+1, tabela, altValues, childAttributes);
            }
        } else {
            //
        }
        return -1;
    }
    
    private boolean razvrstiAtributePoHierarhijiDrevesa() {
        
        //ahaje! tukaj se kreira model!
        //čeprav razvrstitev po hierarhiji ni potrebna. bom pa poskusil za vsakega ki ima
        //otroke potem za tiste otroke ko jih bom prešel dodati piko
        
//        ArrayList<Attribute> attsOrdered = new ArrayList<Attribute>(at.size());
//        //razvrsti po vrsti za evaluacijo
//        for (int i=0; i<at.size(); i++) {
//            if (hasChildren(at.get(i))) {
//                
//            } else {
//                
//            }
//        }
        
        System.out.println("razvrstiAtributePoHierarhijiDrevesa"+" IZVEDENA");
        return true;
    }
    
    private boolean preveriAliImajoAtributiVVsehAlternativahNastavljeneVrednosti() {
        boolean check;
        for (int i=0; i<al.size(); i++) {
            for (int j=0; j<al.get(i).getValues().size(); j++) {
                if (al.get(i).getValues().get(j).getValue().equals(".")) {
                    //tu se vprašam ali ima atribut otroke (takrat je to sprejemljivo, saj se po evaluaciji nastavi glede na utilityfunction
                    check = hasChildren(al.get(i).getValues().get(j).getKey());
                    
                    if (check) {
                        //ima otroke in je ok da je vrednost "."
                    } else {
                        updateInfoLabels("Error! Make sure all Alternatives have values set.",
                                "<html>" + al.get(i).getName() + " has values missing. Example: " + al.get(i).getValues().get(j).getValue() + "</html>");
                        return false;
                    }
                }
            }
        }
        System.out.println("preveriAliImajoAtributiVVsehAlternativahNastavljeneVrednosti"+" IZVEDENA!"+" REZULTAT = TRUE");
        return true;
    }
    
    private boolean preveriAliImajoVsiAtributiScale() {
        for (int i=0; i<at.size(); i++) {
            if (at.get(i).getId_scale() == -1) {
                updateInfoLabels("Error", "<html>Make sure all Attributes have Scale set.<br> " + at.get(i).getName() + " has no Scale set.</html>");
                return false;
            }
        }
        System.out.println("preveriAliImajoVsiAtributiScale"+" IZVEDENA!"+" REZULTAT = TRUE");
        return true;
    }
    
    private boolean preveriAliImajoVsiOcetovskiAtributiNastavljenUtilityFunction() {
        boolean check = false;
        for (int i=0; i<at.size(); i++) {
            if (hasChildren(at.get(i))) {
                for (int j=0; j<uf.size(); j++) {
                    if (at.get(i).getId() == uf.get(j).getId_attribute()) {
                        // vse ok
                        check = true;
                        break;
                    } else {
                        
                    }
                }
                if (check == false) {
                    updateInfoLabels("Error", "<html>Make sure all parent Attributes have UtilityFunction set.<br> " + at.get(i).getName() + " has no UtilityFunction set.</html>");
                    return false;   //ker nima UF
                } else {
                    check = false;
                }
            } else {
                // nič, nima otrok, ne potrebuje UtilityFunction
            }
        }
        System.out.println("preveriAliImajoVsiOcetovskiAtributiNastavljenUtilityFunction"+" IZVEDENA!"+" REZULTAT = TRUE");
        return true;
    }
    
    private void jButton1OKSetUtilFnDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1OKSetUtilFnDialogActionPerformed
        // TODO add your handling code here:
        /* poišči vse otroke(atribute) izbranega atributa ter njihove Scale,
        nato dodeli ta utilityfunction očetovskemu atributu*/
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a = ((Attribute)selectedNode.getUserObject());
        UtilityFunction uF = null;
        int uFindex = -1;
        for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
            if (ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute() == a.getId()) {
                uF = ogrodje.getwModel().getUtilFunctions().get(i);
                for (int j=0; j<uF.getValues().size(); j++) {
                    uF.getValues().set(j, (String) jTable1UtilFunction.getModel().getValueAt(j+1, jTable1UtilFunction.getColumnCount()-1));
                }
                jTextField3UtilityFunction.setText(uF.getDescription());
                jLabel4StatusBarLabel.setText("<html>" + uF.getDescription() + " created" + "</html>");
                ogrodje.getwModel().getUtilFunctions().set(i, uF);
//                System.out.println("uF.getValues.size= "+uF.getValues().size());
//                System.out.println(uF.outputValues());
            }
        }
        jDialog1SetUtilFn.setVisible(false);
    }//GEN-LAST:event_jButton1OKSetUtilFnDialogActionPerformed

    private void jTable1UtilFunctionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1UtilFunctionMouseClicked
        // TODO add your handling code here:
        //prilagodi ComboBox le v primeru, da se je kliknilo na zadnji/desni stolpec
        int vrstica = jTable1UtilFunction.getSelectedRow();
        int stolpec = jTable1UtilFunction.getSelectedColumn();
        if (stolpec == jTable1UtilFunction.getModel().getColumnCount()-1 && vrstica != 0) {
//            String vrednost = "";
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
            int attrID = ((Attribute)selectedNode.getUserObject()).getId();
//            UtilityFunction uF = null;
//            for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
//                if (ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute() == attrID) {
//                    uF = ogrodje.getwModel().getUtilFunctions().get(i);
//                }
//            }
//            vrednost = String.valueOf(jComboBox1UtilFnPickValue.getSelectedItem());
//            uF.getValues().set(jTable1UtilFunction.getSelectedRow(), vrednost);
            jComboBox1UtilFnPickValue.setModel(createMyComboBoxUtilFnModel(vrstica, attrID));
        } else {
            //nič
            jComboBox1UtilFnPickValue.setModel(new DefaultComboBoxModel()); //razveljavi
        }
    }//GEN-LAST:event_jTable1UtilFunctionMouseClicked

    private void jButton2CancelSetUtilFnDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2CancelSetUtilFnDialogActionPerformed
        // TODO add your handling code here:
        jDialog1SetUtilFn.setVisible(false);
    }//GEN-LAST:event_jButton2CancelSetUtilFnDialogActionPerformed

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
        // TODO add your handling code here:
        
        returnValue = jFileChooser1Open.showDialog(this, "Open");
        
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        // TODO add your handling code here:
        //serializiraj workmodel v obstoječo datoteko
        ogrodje.getwModel().setTreeModel((DefaultTreeModel)jTree2.getModel());
        StateHandler.saveWorkModel(ogrodje.getwModel(), ogrodje.getwModel().getCurrentWorkFile().getAbsolutePath());
        jLabel4StatusBarLabel.setText("State saved in a file " + ogrodje.getwModel().getCurrentWorkFile().getName());
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        // TODO add your handling code here:
        //serializiraj workmodel v izbrano datoteko ter shrani pot do nje
        
        returnValue = jFileChooser1Save.showDialog(this, "Save As");
        
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void jButton1ScaleRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ScaleRemoveActionPerformed
        // TODO add your handling code here:
        
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a;
        if (selectedNode != null) {
            a = (Attribute) selectedNode.getUserObject();
            a.setId_scale(-1);
            Scale scaleToDelete = (Scale) jComboBox1ScaleNames.getSelectedItem();
            int lastScaleCounter = 0;
            
            for (int j=0; j<ogrodje.getwModel().getAttributes().size(); j++) {
                // a-a, potrebno
                if (a.getId_scale() == ogrodje.getwModel().getAttributes().get(j).getId_scale()) {
                    ogrodje.getwModel().getAttributes().get(j).setId_scale(-1);
                    jLabel4StatusBarLabel.setText("<html>" + scaleToDelete.getName() + " removed" + "</html>");
                }
                if (scaleToDelete.getId() == ogrodje.getwModel().getAttributes().get(j).getId_scale()) {
                    lastScaleCounter++;
                }
//                if (a.getId() == ogrodje.getwModel().getAttributes().get(j).getId()) {
//                    ogrodje.getwModel().getAttributes().set(j, a);
                    selectedNode.setUserObject(a);
//                }
            }
//            for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
//                if (ogrodje.getwModel().getScales().get(i).getId() == scaleToDelete.getId()) {
//                    
//                    //ogrodje.getwModel().getScales().remove(i);   ne brišem iz seznama ampak sam oodstranim od atributa   //izbrisal Scale iz seznama!
//                }
//            }
            if (lastScaleCounter == 0) {
                for (int i=0; i<ogrodje.getwModel().getScales().size(); i++) {
                    if (ogrodje.getwModel().getScales().get(i).getId() == scaleToDelete.getId()) {
                        ogrodje.getwModel().getScales().remove(i);       //če je zadnja v seznamu in ni pripeta nobenemu drugemu
                        jLabel4StatusBarLabel.setText("<html>" + scaleToDelete.getName() + " deleted" + "</html>");
                    }
                }
            }
            jComboBox1ScaleNames.setModel(createMyComboBoxScaleModel());        //ustvaro nov model
            //tukaj preleti še alternative in v vsaki vsakemu atributu, ki se ujema prilagodi id_scale
            refreshScaleIDtoAllAttributesInAllAlternatives(-1, a);
        }
        refreshAttributes();
    }//GEN-LAST:event_jButton1ScaleRemoveActionPerformed

    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemActionPerformed
        // TODO add your handling code here:
        
        ogrodje.setwModel(new WorkModel());
        jTree2.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("")));
        initComponents2();

        //nastavi nove modele za vse tabele in comboboxe
        jTable1Alternatives.setModel(createMyTableModel());
        jTable1Evaluation.setModel(createMyEvaluationTableModel());

        refreshAttributes();
//        String newWorkFile = ogrodje.getwModel().getCurrentWorkFile();
//        File tempFile = new File
//        newWorkFile.substring(0, newWorkFile.length() - );
        
//        String path = "";
//        try {
//            path = getProgramPath();
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(DEXMethodApplication.class.getName()).log(Level.SEVERE, null, ex);
//        }
        String newFileName = "DEXMethodEvaluation.dexx0";
        ogrodje.getwModel().setCurrentWorkFile(new File(newFileName));
        jLabel4StatusBarLabel.setText("New " + ogrodje.getwModel().getCurrentWorkFile().getName() + " file opened.");
        System.out.println("New File Created: "+ogrodje.getwModel().getCurrentWorkFile().getAbsolutePath());
        setTitle("DEXMethod - " + newFileName);
    }//GEN-LAST:event_newMenuItemActionPerformed

    public static String getProgramPath() throws UnsupportedEncodingException {
        URL url = MOSProjektDEX.class.getProtectionDomain().getCodeSource().getLocation();
        String jarPath = URLDecoder.decode(url.getFile(), "UTF-8");
        String parentPath = new File(jarPath).getParentFile().getPath();
        System.out.println("getProgramPath() method test: output="+parentPath);
        return parentPath;
   }
    
    private void jComboBox1UtilFnPickValueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1UtilFnPickValueActionPerformed
        // TODO add your handling code here:
        
        //preberi vrednost iz dialoga in jo shrani na pravo mesto v UtilFn
        if (jTable1UtilFunction.getSelectedColumn() == jTable1UtilFunction.getModel().getColumnCount()-1) {
            String vrednost = "";
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
            int attrID = ((Attribute)selectedNode.getUserObject()).getId();
            UtilityFunction uF = null;
            for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
                if (ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute() == attrID) {
                    uF = ogrodje.getwModel().getUtilFunctions().get(i);
                }
            }
            vrednost = String.valueOf(jComboBox1UtilFnPickValue.getSelectedItem());
            if (uF.getValues().size() != jTable1UtilFunction.getModel().getRowCount()-1) {
                uF.setValues(new ArrayList<String>(Collections.nCopies(jTable1UtilFunction.getModel().getRowCount()-1, "")));
                uF.getValues().set(jTable1UtilFunction.getSelectedRow()-1, vrednost);
                System.out.println("UtilityFunction value="+vrednost+" at row:"+(jTable1UtilFunction.getSelectedRow()-1)+" set."+" NEW ArrayList of Values created.");
            } else {
                uF.getValues().set(jTable1UtilFunction.getSelectedRow()-1, vrednost);     //VERJETNO SE PRENESE REFERENCA.
                System.out.println("UtilityFunction value="+vrednost+" at row:"+(jTable1UtilFunction.getSelectedRow()-1)+" set.");
            }
            jTable1UtilFunction.getModel().setValueAt(vrednost, jTable1UtilFunction.getSelectedRow(), jTable1UtilFunction.getSelectedColumn());
        } else {
            //nič
        }
        
    }//GEN-LAST:event_jComboBox1UtilFnPickValueActionPerformed

    private void jButton1SetDefaultUtilFnValuesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1SetDefaultUtilFnValuesActionPerformed
        // TODO add your handling code here:
        DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) jTree2.getLastSelectedPathComponent();
        Attribute a = ((Attribute)selectedNode.getUserObject());
        UtilityFunction uF = null;
//        int uFindex = -1;
        for (int i=0; i<ogrodje.getwModel().getUtilFunctions().size(); i++) {
            if (ogrodje.getwModel().getUtilFunctions().get(i).getId_attribute() == a.getId()) {
                uF = ogrodje.getwModel().getUtilFunctions().get(i);
//                uFindex = i;
            }
        }
        String vrednost = "";
        for (int i=0; i<ogrodje.getwModel().getScales().size(); i++ ) {
            if (ogrodje.getwModel().getScales().get(i).getId() == a.getId_scale()) {
                vrednost = ogrodje.getwModel().getScales().get(i).getScaleValuePairs().get(0).getName();
            }
        }
        
        System.out.println("vrednost="+vrednost);
        String temp;
        for (int i=0; i<jTable1UtilFunction.getRowCount()-1; i++) {
            temp = (String) jTable1UtilFunction.getModel().getValueAt(i+1, jTable1UtilFunction.getColumnCount()-1);
//            System.out.println("temp="+temp+" a bo null? potemtakem se je nastavilo na prazen string");
            if (temp == null) {
                temp = "";
            }
            if (temp.equals("")) {
                uF.getValues().set(i, vrednost);
                jTable1UtilFunction.getModel().setValueAt(vrednost, i+1, jTable1UtilFunction.getColumnCount()-1);
            }
        }
//        ogrodje.getwModel().getUtilFunctions().set(uFindex, uF);
//        System.out.println(uF.outputValues());
    }//GEN-LAST:event_jButton1SetDefaultUtilFnValuesActionPerformed

    private void jButton1ReevaluateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ReevaluateActionPerformed
        // TODO add your handling code here:
        /*TUKAJ:
        - preveri ali imajo vsi atributi scale
        - preveri ali imajo vse starševski atributi utility function
        - preveri ali so vse vrednosti določene - pri scale in pri utility function
        - če so vsi pogoji zagotovljeni, glede na vrednosti otrok izpiši vrednosti staršev
        - obarvaj vrednosti root atributa*/
        //JEBA :D
        boolean [] pogoji = new boolean[5];
        for (int i=0; i<pogoji.length; i++)
        pogoji[i] = true;

        refreshReferencesToCollections();
        
        boolean check = true;
        for (int i=0; i<pogoji.length; i++) {
            System.out.println("PREVERJANJE POGOJEV:"+i);
            switch (i) {
                case 0:
                    pogoji[0] = preveriAliImajoVsiAtributiScale();
                    break;
                case 1:
                    pogoji[1] = preveriAliImajoAtributiVVsehAlternativahNastavljeneVrednosti();
                    break;
                case 2:
                    pogoji[2] = preveriAliImajoVsiOcetovskiAtributiNastavljenUtilityFunction();
                    break;
                case 3:
                    pogoji[3] = razvrstiAtributePoHierarhijiDrevesa();
                    break;
                case 4:
                    pogoji[4] = evaluirajOcetovskeAtributeGledeNaUtilityFunkcijeZaVsakoAlternativo();
                    break;
            }
            
            if (pogoji[i] == false) {
                check = false;
                break;
            }
        }
        if (check) {
            jLabel4StatusBarLabel.setText("<html>" + "Evaluation succeeded" + "</html>");
            updateInfoLabels("Evaluation successful!", "Check the results in the table below.");
            System.out.println("!!! EVALUATION SUCCESSFUL !!!");
        } else {
            jLabel4StatusBarLabel.setText("<html>" + "Evaluation failed" + "</html>");
        }

        jTable1Evaluation.setModel(createMyEvaluationTableModel());
        ((DefaultTableModel)jTable1Evaluation.getModel()).fireTableDataChanged();
    }//GEN-LAST:event_jButton1ReevaluateActionPerformed

    private void jFileChooser1OpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser1OpenActionPerformed
        // TODO add your handling code here:
        
        //odpri datoteko in deserializiraj workmodel ter ga naloži in proži vse ostale potrebne metode da aplikacija normalno deluje
        
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser1Open.getSelectedFile();
            String filePath = file.getAbsolutePath();
            System.out.println(filePath);
            
            WorkModel openedWorkModel = StateHandler.readSavedWorkModel(filePath);
            if (openedWorkModel != null) {
                ogrodje.setwModel(openedWorkModel);
                jLabel4StatusBarLabel.setText("Saved file successfully loaded.");
                jTree2.setModel(ogrodje.getwModel().getTreeModel());
                ((DefaultTreeModel)jTree2.getModel()).reload();
                expandAllNodes(0, jTree2.getRowCount());

                //nastavi nove modele za vse tabele in comboboxe
                jTable1Alternatives.setModel(createMyTableModel());
                jTable1Evaluation.setModel(createMyEvaluationTableModel());

                initComponents2();
                refreshAttributes();
                
                ogrodje.getwModel().setCurrentWorkFile(file);
                setTitle("DEXMethod - " + file.getName());
            } else {
                jLabel4StatusBarLabel.setText("Error opening file.");
            }
            
        } else {
            jLabel4StatusBarLabel.setText("No file opened.");
        }
        returnValue = -1;
        System.out.println("jFileChooser1OPENActionPerformed"+" SE JE ZGODIL!");
        
    }//GEN-LAST:event_jFileChooser1OpenActionPerformed

    private void jFileChooser1SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jFileChooser1SaveActionPerformed
        // TODO add your handling code here:
        
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = jFileChooser1Save.getSelectedFile();
            String filePath = "";
            filePath = file.getAbsolutePath();
            System.out.println(filePath);
            
            if (jFileChooser1Save.getSelectedFile().getName().endsWith(".dexx0") || 
                jFileChooser1Save.getSelectedFile().getName().endsWith(".ser")) {
                String koncnicaCeJeNiVpisane = ".dexx0";
                String ext = "";
                int i = filePath.lastIndexOf('.');

                if (i > 0 &&  i < filePath.length() - 1) {
                    ext = filePath.substring(i+1).toLowerCase();
                    System.out.println("Dolžina končnice="+ext.length() + " V IF");
                }
                System.out.println("Dolžina končnice="+ext.length());
                if (ext.length() == 0) {
                    ogrodje.getwModel().setCurrentWorkFile(new File(filePath + koncnicaCeJeNiVpisane));
                } else {
                    ogrodje.getwModel().setCurrentWorkFile(new File(filePath));
                }

                ogrodje.getwModel().setTreeModel((DefaultTreeModel)jTree2.getModel());
                StateHandler.saveWorkModel(ogrodje.getwModel(), ogrodje.getwModel().getCurrentWorkFile().getAbsolutePath());
                jLabel4StatusBarLabel.setText("State saved in a file " + ogrodje.getwModel().getCurrentWorkFile().getName());

                setTitle("DEXMethod - " + file.getName());
            } else {
                jLabel4StatusBarLabel.setText("Wrong extension. Save again.");
            }
        } else {
            jLabel4StatusBarLabel.setText("No file saved.");
        }
        returnValue = -1;
        System.out.println("jFileChooser1SAVEActionPerformed"+" SE JE ZGODIL!");
    }//GEN-LAST:event_jFileChooser1SaveActionPerformed

    private void jFileChooser1SavePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jFileChooser1SavePropertyChange
        // TODO add your handling code here:
        
        if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(evt.getPropertyName())) {
            System.out.println(evt.getPropertyName());
            System.out.println("oldValue " + evt.getOldValue());
            System.out.println("newValue " + evt.getNewValue());
//            JFileChooser chooser = (JFileChooser)evt.getSource();
//            File oldFile = (File)evt.getOldValue();
//            System.out.println("oldFile="+oldFile+" path="+oldFile.getAbsolutePath());
//            File newFile = (File)evt.getNewValue();
//            System.out.println("newFile="+newFile+" path="+newFile.getAbsolutePath());
//            // The selected file should always be the same as newFile
//            File curFile = chooser.getSelectedFile();
//            System.out.println("curFile="+curFile+" path="+curFile.getAbsolutePath());
        }
    }//GEN-LAST:event_jFileChooser1SavePropertyChange

    private void jTextField1AlternativeRenameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1AlternativeRenameActionPerformed
        // TODO add your handling code here:
        //poskrbi da se novo ime alternative ustrezno shrani ter pogled osveži
        String newName = jTextField1AlternativeRename.getText();
        int row = jTable1Alternatives.getSelectedRow();
        int col = jTable1Alternatives.getSelectedColumn();
        refreshReferencesToCollections();
        if (row == 0) {     //morda pogoj da je v prvi vrstici ni potreben... ampak ok
            if (col > 0 && col < al.size()+1) {
                for (int i=0; i<al.size(); i++) {
                    if (i == col-1) {
                        //našel alternativo ; prikaži ime
                        al.get(i).setName(newName);
                        jTable1Alternatives.setValueAt(newName, row, col);
                    }
                }
            }
        }
        
    }//GEN-LAST:event_jTextField1AlternativeRenameActionPerformed
    
    private void updateInfoLabels(String top, String bottom) {
        jLabel4Info1.setText(top);
        jLabel5Info2.setText(bottom);
    }
    
    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Windows".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
////                    SwingUtilities.updateComponentTreeUI();
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(DEXMethodApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new DEXMethodApplication().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentsMenuItem;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JMenuItem cutMenuItem;
    private javax.swing.JMenuItem deleteMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JButton jButton1Add;
    private javax.swing.JButton jButton1AddAlternative;
    private javax.swing.JButton jButton1AddScaleValuePair;
    private javax.swing.JButton jButton1OKSetScaleDialog;
    private javax.swing.JButton jButton1OKSetUtilFnDialog;
    private javax.swing.JButton jButton1Reevaluate;
    private javax.swing.JButton jButton1RemoveAlternative;
    private javax.swing.JButton jButton1ScaleRemove;
    private javax.swing.JButton jButton1SetDefaultUtilFnValues;
    private javax.swing.JButton jButton1SetScale;
    private javax.swing.JButton jButton1SetUtilityFunction;
    private javax.swing.JButton jButton2CancelSetScaleDialog;
    private javax.swing.JButton jButton2CancelSetUtilFnDialog;
    private javax.swing.JButton jButton2Remove;
    private javax.swing.JButton jButton2RemoveScaleValuePair;
    private javax.swing.JComboBox jComboBox1ScaleNames;
    private javax.swing.JComboBox jComboBox1UtilFnPickValue;
    private javax.swing.JComboBox jComboBox2AttributeScaleValues;
    private javax.swing.JDialog jDialog1SetScale;
    private javax.swing.JDialog jDialog1SetUtilFn;
    private javax.swing.JFileChooser jFileChooser1Open;
    private javax.swing.JFileChooser jFileChooser1Save;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3Chart;
    private javax.swing.JLabel jLabel4Info1;
    private javax.swing.JLabel jLabel4StatusBarLabel;
    private javax.swing.JLabel jLabel4UtilFnInfo;
    private javax.swing.JLabel jLabel5Info2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4Chart;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel7Attribute;
    private javax.swing.JPanel jPanel7Scale;
    private javax.swing.JPanel jPanel7UtilityFunction;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1Alternatives;
    private javax.swing.JTable jTable1Evaluation;
    private javax.swing.JTable jTable1SetScale;
    private javax.swing.JTable jTable1UtilFunction;
    private javax.swing.JTextArea jTextArea1TEST;
    private javax.swing.JTextField jTextField1AlternativeRename;
    private javax.swing.JTextField jTextField1AttName;
    private javax.swing.JTextField jTextField2AttDesc;
    private javax.swing.JTextField jTextField3UtilityFunction;
    private javax.swing.JTree jTree2;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JMenuItem pasteMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    // End of variables declaration//GEN-END:variables


}
